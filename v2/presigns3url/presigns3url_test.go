package presigns3url

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	url            string
	expectedBucket string
	expectedKey    string
}

func TestParseS3PublicUrl(t *testing.T) {
	cases := []TestCase{
		{
			url:            "https://bucketName1.s3.amazonaws.com/objectKey1",
			expectedBucket: "bucketName1",
			expectedKey:    "objectKey1",
		},
	}
	for _, c := range cases {
		bucket, key := parseS3PublicUrl(c.url)
		assert.Equal(t, c.expectedBucket, bucket)
		assert.Equal(t, c.expectedKey, key)
	}
}
