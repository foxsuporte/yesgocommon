package presigns3url

import (
	"context"
	"regexp"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type PresignS3UrlInterface interface {
	CreateSignURL(ctx context.Context, mimeType, bucket, key string, timeExpireInMinutes int64) (string, error)
	Presign(ctx context.Context, mimeType, publicUrl string) (string, error)
}

type S3UrlPresigner struct {
	client *s3.PresignClient
}

func NewS3UrlPresigner(cfg aws.Config) PresignS3UrlInterface {
	return &S3UrlPresigner{
		client: s3.NewPresignClient(s3.NewFromConfig(cfg)),
	}
}

func (p *S3UrlPresigner) CreateSignURL(ctx context.Context, mimeType string, bucket string, key string, timeExpireInMinutes int64) (string, error) {
	return p.getObjectUrl(ctx, mimeType, bucket, key, time.Duration(timeExpireInMinutes)*time.Minute)
}

func (p *S3UrlPresigner) getObjectUrl(ctx context.Context, mimeType string, bucket string, key string, expireDuration time.Duration) (string, error) {
	res, err := p.client.PresignGetObject(ctx,
		&s3.GetObjectInput{
			Bucket:              aws.String(bucket),
			Key:                 aws.String(key),
			ResponseContentType: aws.String(mimeType),
		},
		s3.WithPresignExpires(expireDuration),
	)
	if err != nil {
		return "", err
	}
	return res.URL, nil
}

func (p *S3UrlPresigner) Presign(ctx context.Context, mimeType string, publicUrl string) (string, error) {
	bucket, key := parseS3PublicUrl(publicUrl)
	return p.getObjectUrl(ctx, mimeType, bucket, key, 60*time.Minute)
}

func parseS3PublicUrl(s3Url string) (string, string) {
	url := regexp.MustCompile(`^https://([^./]+)\.[^/]+/(.+)`)
	matches := url.FindSubmatch([]byte(s3Url))
	if len(matches) == 3 {
		return string(matches[1]), string(matches[2])
	}
	return "", ""
}
