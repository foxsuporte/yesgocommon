package queuesubscriber

import (
	"context"
	"errors"
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
	"github.com/stretchr/testify/assert"
)

type SqsMock struct {
	simulatedError    error
	simulatedMessages []types.Message
}

func (s SqsMock) ReceiveMessage(ctx context.Context, params *sqs.ReceiveMessageInput, optFns ...func(*sqs.Options)) (*sqs.ReceiveMessageOutput, error) {
	fmt.Println("receiving msg")
	if s.simulatedError != nil {
		return nil, s.simulatedError
	}
	if len(s.simulatedMessages) > 0 {
		return &sqs.ReceiveMessageOutput{
			Messages: s.simulatedMessages,
		}, nil
	}
	return nil, errors.New("MOCK_ERROR - Mock was not setup with any messages or errors")
}

func (s SqsMock) DeleteMessage(ctx context.Context, params *sqs.DeleteMessageInput, optFns ...func(*sqs.Options)) (*sqs.DeleteMessageOutput, error) {
	return &sqs.DeleteMessageOutput{}, nil
}

func TestPoll(t *testing.T) {
	tests := []struct {
		name               string
		mock               SqsMock
		expectedFatalCount int
	}{
		{
			name: "Test queue does not exist error",
			mock: SqsMock{
				simulatedError: errors.New("QueueDoesNotExist"),
			},
			expectedFatalCount: 1,
		},
		{
			name: "Test success",
			mock: SqsMock{
				simulatedMessages: []types.Message{
					{
						Body: aws.String("Hello, world!"),
					},
				},
			},
			expectedFatalCount: 0,
		},
	}

	ctx := context.Background()
	stopQueue := make(chan bool, 1)
	var fatalCount int

	//@override - Do not throw fatal errors while testing
	logFatalf = func(msg string, i ...interface{}) {
		fatalCount++
		stopQueue <- true
	}

	var processMessage = func(string, func()) {
		stopQueue <- true
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			fatalCount = 0
			sqsNew = func(config aws.Config) SqsInterface {
				return tt.mock
			}
			q := NewQueueSubscriber(aws.Config{})
			q.poll(ctx, "", processMessage, stopQueue)
			assert.Equal(t, tt.expectedFatalCount, fatalCount)
		})
	}
}
