package queuesubscriber

import (
	"context"
	"log"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/sqs"
	"github.com/aws/aws-sdk-go-v2/service/sqs/types"
)

const numWorkers = 10

type QueueSubscriberInterface interface {
	StartQueueSubscriber(ctx context.Context, sqsTopicUrl string, processMessageHandle func(string, func())) func()
}

type QueueSubscriber struct {
	client SqsInterface
}

var _ QueueSubscriberInterface = (*QueueSubscriber)(nil)

func NewQueueSubscriber(cfg aws.Config) *QueueSubscriber {
	return &QueueSubscriber{
		client: sqsNew(cfg),
	}
}

var sqsNew = SqsNew

func SqsNew(cfg aws.Config) SqsInterface {
	return sqs.NewFromConfig(cfg)
}

type SqsInterface interface {
	ReceiveMessage(ctx context.Context, params *sqs.ReceiveMessageInput, optFns ...func(*sqs.Options)) (*sqs.ReceiveMessageOutput, error)
	DeleteMessage(ctx context.Context, params *sqs.DeleteMessageInput, optFns ...func(*sqs.Options)) (*sqs.DeleteMessageOutput, error)
}

var logFatalf = log.Fatalf

func (q *QueueSubscriber) StartQueueSubscriber(
	ctx context.Context,
	sqsTopicUrl string,
	processMessageHandle func(string, func()),
) func() {
	log.Println("[SQS] Starting queue subscriber")
	stopQueue := make(chan bool)

	for i := 0; i < numWorkers; i++ {
		go q.poll(ctx, sqsTopicUrl, processMessageHandle, stopQueue)
	}

	return func() {
		close(stopQueue)
	}
}

func (q *QueueSubscriber) poll(ctx context.Context, sqsTopicUrl string, processMessageHandle func(string, func()), stopQueue chan bool) {
	for {
		select {
		case <-stopQueue:
			return
		case <-ctx.Done():
			return
		default:
			q.receiveAndProcessMessages(ctx, sqsTopicUrl, processMessageHandle)
		}
	}
}

func (q *QueueSubscriber) receiveAndProcessMessages(ctx context.Context, sqsTopicUrl string, processMessageHandle func(string, func())) {
	// Receive a message from the SQS queue with long polling enabled.
	result, err := q.client.ReceiveMessage(ctx, &sqs.ReceiveMessageInput{
		QueueUrl: &sqsTopicUrl,
		MessageSystemAttributeNames: []types.MessageSystemAttributeName{
			"SentTimestamp",
		},
		MaxNumberOfMessages: 1,
		MessageAttributeNames: []string{
			"All",
		},
		WaitTimeSeconds: int32(20),
	})
	if err != nil {
		logFatalf("[SQS] Error receiving message from SQS queue: %s", err.Error())
		return
	}

	if len(result.Messages) > 0 {
		log.Printf("[SQS] %d new messages received.\n", len(result.Messages))
		for i := range result.Messages {
			q.processMessage(ctx, sqsTopicUrl, result.Messages[i], processMessageHandle)
		}
	}
}

func (q *QueueSubscriber) processMessage(ctx context.Context, sqsTopicUrl string, msg types.Message, processMessageHandle func(string, func())) {
	successHandle := func() {
		log.Printf("[SQS] Deleting message from queue...\n")
		_, err := q.client.DeleteMessage(ctx, &sqs.DeleteMessageInput{
			QueueUrl:      &sqsTopicUrl,
			ReceiptHandle: msg.ReceiptHandle,
		})
		if err != nil {
			log.Printf("[SQS] Error deleting message from queue: %s\n", err.Error())
		}
		log.Println("[SQS] Message has been deleted from queue")
	}
	processMessageHandle(*msg.Body, successHandle)
}
