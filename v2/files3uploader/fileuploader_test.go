package files3uploader

import (
	"context"
	"testing"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const testURL = "TEST_URL"

func mockUpload(context.Context, *AwsS3FileUploader, *s3.PutObjectInput) (*s3.PutObjectOutput, error) {
	return &s3.PutObjectOutput{}, nil
}

func mockGetFileUrl(context.Context, *AwsS3FileUploader, string) (string, error) {
	return testURL, nil
}

func Test(t *testing.T) {
	upload = mockUpload
	getFileUrl = mockGetFileUrl

	uploader := NewAwsS3FileUploader(aws.Config{}, "")
	url, err := uploader.Upload(context.Background(), "TEST_FILEPATH", []byte{})
	require.NoError(t, err)
	assert.Equal(t, testURL, url)
}
