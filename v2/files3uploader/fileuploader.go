package files3uploader

import (
	"bytes"
	"context"
	"fmt"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/service/s3"
)

type FileUploaderInterface interface {
	Upload(ctx context.Context, filePath string, fileContents []byte) (url string, err error)
}

type AwsS3FileUploader struct {
	bucket string

	client        *s3.Client
	presignClient *s3.PresignClient
}

func NewAwsS3FileUploader(cfg aws.Config, awsBucket string) FileUploaderInterface {
	client := s3.NewFromConfig(cfg,
		func(o *s3.Options) {
			o.UsePathStyle = true
		},
	)

	return &AwsS3FileUploader{
		bucket:        awsBucket,
		client:        client,
		presignClient: s3.NewPresignClient(client),
	}
}

var (
	upload     = s3Upload
	getFileUrl = s3GetFileUrl
)

func s3Upload(ctx context.Context, f *AwsS3FileUploader, uploadInput *s3.PutObjectInput) (*s3.PutObjectOutput, error) {
	return f.client.PutObject(ctx, uploadInput)
}

func s3GetFileUrl(ctx context.Context, f *AwsS3FileUploader, filePath string) (string, error) {
	return f.getFileUrl(ctx, filePath)
}

func (f *AwsS3FileUploader) Upload(ctx context.Context, filePath string, fileContents []byte) (url string, err error) {
	if err = f.upload(ctx, filePath, fileContents); err != nil {
		return "", err
	}
	return getFileUrl(ctx, f, filePath)
}

func (f *AwsS3FileUploader) upload(ctx context.Context, filePath string, fileContents []byte) error {
	uploadInput := &s3.PutObjectInput{
		Bucket: aws.String(f.bucket),
		Key:    aws.String(filePath),
		Body:   bytes.NewReader(fileContents),
	}
	_, err := upload(ctx, f, uploadInput)
	if err != nil {
		return fmt.Errorf("failed to upload file, %v", err)
	}
	return nil
}

func (f *AwsS3FileUploader) getFileUrl(ctx context.Context, filePath string) (string, error) {
	result, err := f.presignClient.PresignGetObject(ctx, &s3.GetObjectInput{
		Bucket: aws.String(f.bucket),
		Key:    aws.String(filePath),
	})
	if err != nil {
		return "", fmt.Errorf("failed to get file url, %v", err)
	}
	return result.URL, nil
}
