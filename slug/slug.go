package slug

import (
	"regexp"
	"strings"
)

var (
	regexpNonAuthorizedChars = regexp.MustCompile("[^a-z0-9-]")
	regexpMultipleDashes     = regexp.MustCompile("-+")
)

// Slug converts the string passed by parameter to a slug representation
func Slug(s string) (slug string) {
	slug = strings.ToLower(s)
	slug = regexpNonAuthorizedChars.ReplaceAllString(slug, "-")
	slug = regexpMultipleDashes.ReplaceAllString(slug, "-")
	return strings.Trim(slug, "-")
}
