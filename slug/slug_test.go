package slug

import (
	"fmt"
	"testing"
)

func TestSlug(t *testing.T) {
	scenarios := []struct {
		actual   string
		expected string
	}{
		{
			actual:   "FOO BAR BAZ",
			expected: "foo-bar-baz",
		},
		{
			actual:   "Foo--Bar--Baz",
			expected: "foo-bar-baz",
		},
		{
			actual:   "Foo+bar+baz",
			expected: "foo-bar-baz",
		},
		{
			actual:   "Foo_bar_baz",
			expected: "foo-bar-baz",
		},
		{
			actual:   "Foo©bar©baz",
			expected: "foo-bar-baz",
		},
	}

	for _, s := range scenarios {
		if s.expected != Slug(s.actual) {
			t.Errorf("Slug(%q) expected %q and got %q", s.actual, s.expected, Slug(s.actual))
		}
	}
}

func ExampleSlug() {
	fmt.Println(Slug("FOO BAR BAZ"))
	fmt.Println(Slug("foo--bar--baz"))
	fmt.Println(Slug("foo_bar+baz"))
	// Output:
	// foo-bar-baz
	// foo-bar-baz
	// foo-bar-baz
}
