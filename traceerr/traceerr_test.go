package traceerr_test

import (
	"errors"
	"testing"

	"bitbucket.org/foxsuporte/yesgocommon/traceerr"
	"github.com/stretchr/testify/assert"
)

// docker compose exec app tests --run ^TestWrapError$ -v
func TestWrapError(t *testing.T) {
	sourceErr := errors.New("some error")

	t.Run("sourceErr is not typed as traceerr.Error", func(t *testing.T) {
		err := traceerr.WrapError(sourceErr)
		parsedErr, ok := err.(*traceerr.Error)
		assert.True(t, ok)
		assert.Equal(t, sourceErr, parsedErr.ParentErr)

		errMsg := err.Error()
		assert.NotEmpty(t, errMsg)
		assert.Contains(t, errMsg, sourceErr.Error())
		assert.Contains(t, errMsg, "| location:")
		assert.Contains(t, errMsg, ".go:")

		parsedErrMsg := err.Error()
		assert.NotEmpty(t, parsedErrMsg)
		assert.Contains(t, parsedErrMsg, sourceErr.Error())
		assert.Contains(t, parsedErrMsg, "| location:")
		assert.Contains(t, parsedErrMsg, ".go:")
	})

	t.Run("sourceErr is typed as ErrorWithTrace", func(t *testing.T) {
		err := traceerr.WrapError(sourceErr)
		err2 := traceerr.WrapError(err)
		assert.Equal(t, err, err2)
	})

}

// docker compose exec app tests --run ^TestWrapErrorAndPrintLog$ -v
func TestWrapErrorAndPrintLog(t *testing.T) {
	sourceErr := errors.New("some error")

	t.Run("sourceErr is typed as ErrorWithTrace and log ", func(t *testing.T) {
		err := traceerr.WrapErrorAndPrintLog(sourceErr)
		err2 := traceerr.WrapError(err)
		assert.Equal(t, err, err2)
	})
}

// docker compose exec app tests --run ^TestWrapErrorf$ -v
func TestWrapErrorf(t *testing.T) {
	t.Run("sourceErr is not typed as traceerr.Error", func(t *testing.T) {
		err := traceerr.WrapErrorf("formatted error %d %s", 42, "foo")
		parsedErr, ok := err.(*traceerr.Error)
		assert.True(t, ok)
		assert.Equal(t, "formatted error 42 foo", parsedErr.ParentErr.Error())

		errMsg := err.Error()
		assert.NotEmpty(t, errMsg)
		assert.Contains(t, errMsg, "formatted error 42 foo")
		assert.Contains(t, errMsg, "| location:")
		assert.Contains(t, errMsg, ".go:")

		parsedErrMsg := err.Error()
		assert.NotEmpty(t, parsedErrMsg)
		assert.Contains(t, parsedErrMsg, "formatted error 42 foo")
		assert.Contains(t, parsedErrMsg, "| location:")
		assert.Contains(t, parsedErrMsg, ".go:")
	})

}

// docker compose exec app tests --run ^TestWrapErrorfAndPrintLog$ -v
func TestWrapErrorfAndPrintLog(t *testing.T) {
	t.Run("sourceErr is not typed as traceerr.Error and log", func(t *testing.T) {
		err := traceerr.WrapErrorfAndPrintLog("formatted error %d %s", 42, "foo")
		parsedErr, ok := err.(*traceerr.Error)
		assert.True(t, ok)
		assert.Equal(t, "formatted error 42 foo", parsedErr.ParentErr.Error())

		errMsg := err.Error()
		assert.NotEmpty(t, errMsg)
		assert.Contains(t, errMsg, "formatted error 42 foo")
		assert.Contains(t, errMsg, "| location:")
		assert.Contains(t, errMsg, ".go:")

		parsedErrMsg := err.Error()
		assert.NotEmpty(t, parsedErrMsg)
		assert.Contains(t, parsedErrMsg, "formatted error 42 foo")
		assert.Contains(t, parsedErrMsg, "| location:")
		assert.Contains(t, parsedErrMsg, ".go:")
	})
}

// docker compose exec app tests --run ^TestError$ -v
func TestError(t *testing.T) {
	sourceErr := errors.New("some error")
	err := traceerr.WrapError(sourceErr)
	errMsg := err.Error()

	assert.NotEmpty(t, errMsg)
	assert.Contains(t, errMsg, sourceErr.Error())
	assert.Contains(t, errMsg, "| location:")
	assert.Contains(t, errMsg, ".go:")
}

// docker compose exec app tests --run ^TestWrapErrorFileLocation$ -v
func TestWrapErrorFileLocation(t *testing.T) {
	sourceErr := errors.New("some error")

	wrapErr := traceerr.WrapError(sourceErr)
	assert.ErrorContains(t, wrapErr, "some error | location:")
	assert.ErrorContains(t, wrapErr, "/traceerr/traceerr_test.go:")
	wrapAndPrintErr := traceerr.WrapErrorAndPrintLog(sourceErr)
	assert.ErrorContains(t, wrapAndPrintErr, "some error | location:")
	assert.ErrorContains(t, wrapAndPrintErr, "/traceerr/traceerr_test.go:")

	wrapErrF := traceerr.WrapErrorf("some %s", "error")
	assert.ErrorContains(t, wrapErrF, "some error | location:")
	assert.ErrorContains(t, wrapErrF, "/traceerr/traceerr_test.go:")
	wrapAndPrintErrF := traceerr.WrapErrorfAndPrintLog("some %s", "error")
	assert.ErrorContains(t, wrapAndPrintErrF, "some error | location:")
	assert.ErrorContains(t, wrapAndPrintErrF, "/traceerr/traceerr_test.go:")
}
