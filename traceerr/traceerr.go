package traceerr

import (
	"fmt"
	"log"
	"runtime"
	"strconv"
)

// Error é um tipo personalizado do pkg 'traceerr' que inclui location com informações de arquivo e linha.
type Error struct {
	location  string
	caller    int64
	ParentErr error
}

func (err *Error) Error() string {
	return fmt.Sprintf("%v | location:%s", err.ParentErr.Error(), err.location)
}

func wrapErrorf(format string, a ...interface{}) error {
	_, file, line, _ := runtime.Caller(2)
	return &Error{ParentErr: fmt.Errorf(format, a...), location: file + ":" + strconv.Itoa(line), caller: 2}
}

func WrapErrorf(format string, a ...interface{}) error {
	return wrapErrorf(format, a...)
}

func wrapError(err error) error {
	_, ok := err.(*Error)
	if ok {
		return err
	}
	_, file, line, _ := runtime.Caller(2)
	return &Error{ParentErr: err, location: file + ":" + strconv.Itoa(line), caller: 2}
}

func WrapError(err error) error {
	return wrapError(err)
}

func WrapErrorAndPrintLog(err error) error {
	wrapError := wrapError(err)
	log.Println(wrapError)
	return wrapError
}

func WrapErrorfAndPrintLog(format string, a ...interface{}) error {
	wrapError := wrapErrorf(format, a...)
	log.Println(wrapError)
	return wrapError
}
