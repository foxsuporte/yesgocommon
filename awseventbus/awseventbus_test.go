package awseventbus

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"testing"
	"time"
	eh "github.com/looplab/eventhorizon"
)

type EventDispatcherMock struct {
	c chan map[string]interface{}
}

func (e EventDispatcherMock) Dispatch(data map[string]interface{}) error {
	fmt.Println("Dispatching event")
	e.c <- data
	return nil
}

type QueueSubscriberMock struct {
	c       <-chan map[string]interface{}
	Handled bool
}

func (q *QueueSubscriberMock) StartQueueSubscriber(sqsTopicUrl string, processMessageHandle func(string, func())) func() {
	fmt.Println("Starting queue subscriber")
	stopSignal := false
	go func() {
		for data := range q.c {
			encodedInner, err := json.Marshal(data)
			if err != nil {
				log.Fatal("encodedInner: ", err)
			}
			encoded, err := json.Marshal(map[string]interface{}{
				"Message": string(encodedInner),
			})
			if err != nil {
				log.Fatal("encoded: ", err)
			}
			fmt.Println("encoded: ", string(encoded))
			processMessageHandle(string(encoded), func() {
				q.Handled = true
				fmt.Println("Successfully processed message")
			})
			if stopSignal {
				break
			}
		}
	}()
	return func() {
		stopSignal = true
	}
}

type EventHandlerMock struct {
	Called bool
}

func (EventHandlerMock) HandlerType() eh.EventHandlerType {
	return eh.EventHandlerType("MockEventHandlerType")
}

func (e EventHandlerMock) HandleEvent(context.Context, eh.Event) error {
	e.Called = true
	fmt.Println("Handling event")
	return nil
}

type CollectionMock struct{}

func (CollectionMock) Remove(interface{}) error {
	return nil
}

/*
type MockEvent struct{}

func (e *MockEvent) EventType() eh.EventType         { return "MockEvent" }
func (e *MockEvent) Data() eh.EventData              { return "MyData" }
func (e *MockEvent) Timestamp() time.Time            { return time.Now() }
func (e *MockEvent) AggregateType() eh.AggregateType { return "MyAggregate" }
func (e *MockEvent) AggregateID() uuid.UUID          { return uuid.New() }
func (e *MockEvent) Version() int                    { return 0 }
func (e *MockEvent) String() string                  { return "MockEventAsString" }
*/

func TestSqsTopicNotConfigured(t *testing.T) {
	eventHandler := &EventHandlerMock{}
	sqsTopics := map[eh.EventHandler]string{}
	bus := NewAwsEventBus(EventDispatcherMock{}, sqsTopics, &QueueSubscriberMock{}, CollectionMock{})
	called := false
	oldLogFatalf := logFatalf
	logFatalf = func(msg string, i ...interface{}) {
		called = true
	}
	bus.AddHandler(eh.MatchAny(), eventHandler)
	logFatalf = oldLogFatalf
	if !called {
		t.Error("Did not call log.Fatalf when running with SQS config errors")
	}
}

func TestSuccessProcessing(t *testing.T) {
	// replace createEventData func to avoid "if error"
	createEventData = func(eventType eh.EventType) (eh.EventData, error) {
		data, _ := eh.CreateEventData(eventType)
		return data, nil
	}

	eventHandler := &EventHandlerMock{}      //Cria eventHandler mock
	sqsTopics := map[eh.EventHandler]string{ // Cria sqsTopics com o eventHandlerMock
		eventHandler: "",
	}
	c := make(chan map[string]interface{})
	queueSubscriber := &QueueSubscriberMock{c, false}
	bus := NewAwsEventBus(EventDispatcherMock{c}, sqsTopics, queueSubscriber, CollectionMock{})
	bus.AddHandler(eh.MatchAny(), eventHandler)
	evt := eh.NewEvent(eh.EventType("MockEvent"), "MyData", time.Now())
	err := bus.PublishEvent(context.Background(), evt)
	bus.syncPublishEvent(context.Background(), evt)
	waitingForHandling(queueSubscriber)
	if err != nil {
		t.Error(err)
	}
}

func TestEventMatchDontHandle(t *testing.T) {
	// replace createEventData func to avoid "if error"
	createEventData = func(eventType eh.EventType) (eh.EventData, error) {
		data, _ := eh.CreateEventData(eventType)
		return data, nil
	}

	eventHandler := &EventHandlerMock{}      //Cria eventHandler mock
	sqsTopics := map[eh.EventHandler]string{ // Cria sqsTopics com o eventHandlerMock
		eventHandler: "",
	}
	c := make(chan map[string]interface{})
	queueSubscriber := &QueueSubscriberMock{c, false}
	bus := NewAwsEventBus(EventDispatcherMock{c}, sqsTopics, queueSubscriber, CollectionMock{})
	bus.AddHandler(eh.MatchEvent("InvalidEventType"), eventHandler)
	evt := eh.NewEvent(eh.EventType("MockEvent"), "MyData", time.Now())

	err := bus.PublishEvent(context.Background(), evt)
	if err != nil {
		log.Println(err.Error())
	}

	bus.syncPublishEvent(context.Background(), evt)

	waitingForHandling(queueSubscriber)

	if eventHandler.Called {
		t.Errorf("should not call HandleEvent for \"InvalidEventType\"")
	}
}

func waitingForHandling(q *QueueSubscriberMock) {
	start := time.Now().Unix()
	for {
		if time.Now().Unix()-start > TIMEOUT {
			fmt.Println("timed out waiting for handling")
			break
		}
		time.Sleep(time.Millisecond * 1)
		if q.Handled {
			break
		}
	}
}
