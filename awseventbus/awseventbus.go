package awseventbus

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/google/uuid"
	eh "github.com/looplab/eventhorizon"
)

const TIMEOUT = 20

var logFatalf = log.Fatalf

type DeletableCollectionInterface interface {
	Remove(interface{}) error
}

type QueueSubscriberInterface interface {
	StartQueueSubscriber(sqsTopicUrl string, processMessageHandle func(string, func())) func()
}

type EventDispatcherInterface interface {
	Dispatch(map[string]interface{}) error
}

type AwsEventBus struct {
	eventDispatcher             EventDispatcherInterface
	sqsTopics                   map[eh.EventHandler]string
	queueSubscriber             QueueSubscriberInterface
	errorChannel                <-chan eh.EventBusError
	unpublishedEventsCollection DeletableCollectionInterface
}

func NewAwsEventBus(
	eventDispatcher EventDispatcherInterface,
	sqsHandlerTopic map[eh.EventHandler]string,
	queueSubscriber QueueSubscriberInterface,
	unpublishedEventsCollection DeletableCollectionInterface,
) *AwsEventBus {
	c := make(chan eh.EventBusError)
	return &AwsEventBus{eventDispatcher, sqsHandlerTopic, queueSubscriber, c, unpublishedEventsCollection}
}

func (a AwsEventBus) Errors() <-chan eh.EventBusError {
	return a.errorChannel
}

var createEventData = func(eventType eh.EventType) (eh.EventData, error) {
	return eh.CreateEventData(eventType)
}

func getHandleFunc(em eh.EventMatcher, h eh.EventHandler) func(string, func()) {
	return func(message string, successHandle func()) {
		//fmt.Printf("Received message in handler: %s\n", message)
		var s SqsMessage
		err := json.Unmarshal([]byte(message), &s)
		if err != nil {
			fmt.Printf("Error unmarshalling SQS message: %s\n", err.Error())
			return
		}
		var m Message
		err = json.Unmarshal([]byte(s.Message), &m)
		if err != nil {
			fmt.Printf("Error unmarshalling SQS event: %s\n", err.Error())
			return
		}
		e := m.Event

		data, err := createEventData(e.EventType)

		if err != nil {
			fmt.Printf("Error creating initial event data: %s - %+v\n", err.Error(), e)
			return
		}

		contents, err := json.Marshal(e.Data)

		if err != nil {
			fmt.Printf("Error marshalling event data while converting to correct type: %s - %+v\n", err.Error(), e)
			return
		}

		err = json.Unmarshal(contents, &data)
		if err != nil {
			log.Println(err.Error())
			return
		}

		ctx := eh.NewContextWithNamespace(context.Background(), "eventsourcing")

		start := time.Now().Unix()
		var lastErrorMsg string
		go func() {
			for {
				ev := eh.NewEventForAggregate(
					e.EventType,
					data,
					e.Timestamp,
					e.AggregateType,
					e.AggregateID,
					e.Version,
				)
				if !em(ev) { // If event doesn't match, discard it and call successHandle func
					successHandle()
					break
				}
				if time.Now().Unix()-start > TIMEOUT {
					fmt.Println(lastErrorMsg)
					break
				}
				err := h.HandleEvent(ctx, ev)
				if err != nil {
					lastErrorMsg = fmt.Sprintf("[Handler %v] Error handling event: %s - %+v\n", h.HandlerType(), err.Error(), e)
					time.Sleep(100 * time.Millisecond)
					continue
				}
				successHandle()
				break
			}
		}()
	}
}

func (a AwsEventBus) AddHandler(em eh.EventMatcher, h eh.EventHandler) {
	sqsTopic, ok := a.sqsTopics[h]
	if !ok {
		logFatalf("Error: SQS topic not configured for handler %s\n", h.HandlerType())
	}
	handle := getHandleFunc(em, h)

	fmt.Printf("Starting queue subscriber for handler %+v\n", string(h.HandlerType()))
	go a.queueSubscriber.StartQueueSubscriber(sqsTopic, handle)
}

func (a AwsEventBus) AddObserver(m eh.EventMatcher, h eh.EventHandler) {
	log.Println("Warning: awseventbus does not support observers")
}

func (a AwsEventBus) PublishEvent(ctx context.Context, e eh.Event) error {
	go a.syncPublishEvent(ctx, e)
	return nil
}

func (a AwsEventBus) syncPublishEvent(ctx context.Context, e eh.Event) {
	data := map[string]interface{}{
		"Event": Event{
			EventType:     e.EventType(),
			Data:          e.Data(),
			Timestamp:     e.Timestamp(),
			AggregateType: e.AggregateType(),
			AggregateID:   e.AggregateID(),
			Version:       e.Version(),
		},
	}
	err := a.eventDispatcher.Dispatch(data)
	if err == nil {
		// On success dispatching to SNS, delete event from unpublished collection
		err := a.unpublishedEventsCollection.Remove(map[string]interface{}{
			"event._id":            e.AggregateID().String(),
			"event.aggregate_type": e.AggregateType(),
			"event.version":        e.Version(),
			"event.event_type":     e.EventType(),
		})
		if err != nil {
			log.Println(err.Error())
		}
	}
}

type SqsMessage struct {
	Message string
}

type Message struct {
	Event Event
}

type Event struct {
	EventType     eh.EventType
	Data          eh.EventData
	Timestamp     time.Time
	AggregateType eh.AggregateType
	AggregateID   uuid.UUID
	Version       int
}
