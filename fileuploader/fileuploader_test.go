package fileuploader

import (
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"testing"
)

func mockUpload(f *AwsS3FileUploader, uploadInput *s3manager.UploadInput) (*s3manager.UploadOutput, error) {
	return &s3manager.UploadOutput{
		Location: "TEST_LOCATION",
	}, nil
}

func Test(t *testing.T) {
	upload = mockUpload
	uploader := NewAwsS3FileUploader("", "", "", "")
	url, err := uploader.Upload("TEST_FILEPATH", []byte{})
	if err != nil {
		t.Error("Error testing fileuploader", err.Error())
	}
	if url != "TEST_LOCATION" {
		t.Error("Error testing fileuploader. Expected location TEST_LOCATION. Got", url)
	}
}
