package fileuploader

import (
	"bytes"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"log"
)

type FileUploaderInterface interface {
	Upload(string, []byte) (url string, err error)
}

type AwsS3FileUploader struct {
	AwsSecret         string
	AwsKey            string
	AwsRegion         string
	AwsBucket         string
	S3ManagerUploader *s3manager.Uploader
}

func NewAwsS3FileUploader(awsSecret, awsKey, awsRegion, awsBucket string) FileUploaderInterface {

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(awsRegion),
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
	})

	if err != nil {
		log.Fatal(err)
	}

	svc := s3.New(sess)

	// Create an uploader with S3 client and default options
	uploader := s3manager.NewUploaderWithClient(svc)

	return &AwsS3FileUploader{
		awsSecret,
		awsKey,
		awsRegion,
		awsBucket,
		uploader,
	}
}

var upload = S3Upload

func S3Upload(f *AwsS3FileUploader, uploadInput *s3manager.UploadInput) (*s3manager.UploadOutput, error) {
	return f.S3ManagerUploader.Upload(uploadInput)
}

func (f *AwsS3FileUploader) Upload(filePath string, fileContents []byte) (url string, err error) {
	uploadInput := &s3manager.UploadInput{
		Bucket: aws.String(f.AwsBucket),
		Key:    aws.String(filePath),
		Body:   bytes.NewReader(fileContents),
	}
	result, err := upload(f, uploadInput)
	if err != nil {
		return "", fmt.Errorf("failed to upload file, %v", err)
	}
	return result.Location, nil
}
