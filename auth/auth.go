package auth

import (
	"context"
	"net/http"
)

const HTTP_HEADER_USER_EMAIL = "SECURITY_TOKEN_USER_EMAIL"
const HTTP_HEADER_USER_ID = "SECURITY_TOKEN_USER_ID"
const keyUserEmail = 0
const keyUserId = 1

func FromRequest(r *http.Request) (userEmail string, userId string) {
	return r.Header.Get(HTTP_HEADER_USER_EMAIL), r.Header.Get(HTTP_HEADER_USER_ID)
}

func FromContext(ctx context.Context) (userEmail string, userId string) {

	rawUserEmail := ctx.Value(keyUserEmail)
	if rawUserEmail != nil {
		userEmail = rawUserEmail.(string)
	}

	rawUserId := ctx.Value(keyUserId)
	if rawUserId != nil {
		userId = rawUserId.(string)
	}

	return userEmail, userId
}

func NewContext(ctx1 context.Context, userEmail, userId string) context.Context {
	ctx2 := context.WithValue(ctx1, keyUserEmail, userEmail)
	ctx3 := context.WithValue(ctx2, keyUserId, userId)
	return ctx3
}

func MiddlewareHandler(inner http.Handler) http.Handler {
	handler := func(w http.ResponseWriter, r *http.Request) {
		userEmail, userId := FromRequest(r)
		ctx := NewContext(r.Context(), userEmail, userId)
		inner.ServeHTTP(w, r.WithContext(ctx))
	}
	return http.HandlerFunc(handler)
}
