package presigns3url

import (
	"testing"
)

type TestCase struct {
	url            string
	expectedBucket string
	expectedKey    string
}

func TestParseS3PublicUrl(t *testing.T) {
	cases := []TestCase{
		TestCase{
			url:            "https://bucketName1.s3.amazonaws.com/objectKey1",
			expectedBucket: "bucketName1",
			expectedKey:    "objectKey1",
		},
	}
	for _, c := range cases {
		bucket, key := parseS3PublicUrl(c.url)
		if bucket != c.expectedBucket {
			t.Fatalf("Expected: %s. Got: %s.\n", c.expectedBucket, bucket)
		}
		if key != c.expectedKey {
			t.Fatalf("Expected: %s. Got: %s.\n", c.expectedKey, key)
		}
	}
}
