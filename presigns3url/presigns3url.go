package presigns3url

import (
	"log"
	"regexp"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/s3"
)

type PresignS3UrlInterface interface {
	Presign(client.ConfigProvider, string, string) string
	CreateSignURL(client.ConfigProvider, string, string, string, int64) string
}

type S3UrlPresigner struct {
}

func NewS3UrlPresigner() PresignS3UrlInterface {
	return &S3UrlPresigner{}
}

func (p *S3UrlPresigner) CreateSignURL(awsConfig client.ConfigProvider, mimeType string, bucket string, key string, timeExpireInMinutes int64) string {

	svc := s3.New(awsConfig)

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket:              aws.String(bucket),
		Key:                 aws.String(key),
		ResponseContentType: aws.String(mimeType),
	})

	durantionExpire := time.Duration(timeExpireInMinutes) * time.Minute
	urlStr, err := req.Presign(durantionExpire)

	if err != nil {
		log.Println("Failed to sign request", err)
	}

	return urlStr
}

func (p *S3UrlPresigner) Presign(awsConfig client.ConfigProvider, mimeType string, publicUrl string) string {

	bucket, key := parseS3PublicUrl(publicUrl)

	svc := s3.New(awsConfig)

	req, _ := svc.GetObjectRequest(&s3.GetObjectInput{
		Bucket:              aws.String(bucket),
		Key:                 aws.String(key),
		ResponseContentType: aws.String(mimeType),
	})
	urlStr, err := req.Presign(60 * time.Minute)

	if err != nil {
		log.Println("Failed to sign request", err)
	}

	return urlStr
}

func parseS3PublicUrl(s3Url string) (string, string) {
	url := regexp.MustCompile(`^https://([^./]+)\.[^/]+/(.+)`)
	matches := url.FindSubmatch([]byte(s3Url))
	if len(matches) == 3 {
		return string(matches[1]), string(matches[2])
	}
	return "", ""
}
