package waitablebus

import (
	"context"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/google/uuid"
	eh "github.com/looplab/eventhorizon"
)

// DefaultQueueSize is the default queue size per handler for publishing events.
var DefaultQueueSize = 10000

// EventBus is a local event bus that delegates handling of published events
// to all matching registered handlers, in order of registration.
type EventBus struct {
	group        *Group
	registered   map[eh.EventHandlerType]struct{}
	registeredMu sync.RWMutex
	errCh        chan eh.EventBusError
	wg           sync.WaitGroup
}

// NewEventBus creates a EventBus.
func NewEventBus(g *Group) *EventBus {
	pendingMessages := &PendingMessages{}
	if g == nil {
		g = NewGroup(pendingMessages)
	}
	return &EventBus{
		group:      g,
		registered: map[eh.EventHandlerType]struct{}{},
		errCh:      make(chan eh.EventBusError, 100),
	}
}

// PublishEvent implements the PublishEvent method of the eventhorizon.EventBus interface.
func (b *EventBus) PublishEvent(ctx context.Context, event eh.Event) error {
	b.group.publish(ctx, event)
	return nil
}

// AddHandler implements the AddHandler method of the eventhorizon.EventBus interface.
func (b *EventBus) AddHandler(m eh.EventMatcher, h eh.EventHandler) {
	ch := b.channel(m, h, false)
	go b.handle(m, h, ch)
}

// AddObserver implements the AddObserver method of the eventhorizon.EventBus interface.
func (b *EventBus) AddObserver(m eh.EventMatcher, h eh.EventHandler) {
	ch := b.channel(m, h, true)
	go b.handle(m, h, ch)
}

// Errors implements the Errors method of the eventhorizon.EventBus interface.
func (b *EventBus) Errors() <-chan eh.EventBusError {
	return b.errCh
}

// Handles all events coming in on the channel.
func (b *EventBus) handle(m eh.EventMatcher, h eh.EventHandler, ch <-chan evt) {
	b.wg.Add(1)
	defer b.wg.Done()

	for e := range ch {
		if !m(e.event) {
			b.group.pendingMessages.Lock()
			b.group.pendingMessages.Dec()
			b.group.pendingMessages.Unlock()
			continue
		}
		if err := h.HandleEvent(e.ctx, e.event); err != nil {
			select {
			case b.errCh <- eh.EventBusError{Err: fmt.Errorf("could not handle event (%s): %s", h.HandlerType(), err.Error()), Ctx: e.ctx, Event: e.event}:
			default:
			}
		}
		b.group.pendingMessages.Lock()
		b.group.pendingMessages.Dec()
		b.group.pendingMessages.Unlock()
	}
}

func (b *EventBus) IsEmpty() bool {
	return b.group.pendingMessages.IsEmpty()
}

func (b *EventBus) WaitUntilEmpty(timeoutSeconds int) {
	start := time.Now()
	for {
		if time.Since(start) > time.Duration(timeoutSeconds)*time.Second {
			log.Fatal("Timed out while waiting for event bus to become empty")
		}
		if b.group.pendingMessages.IsEmpty() {
			break
		}
		time.Sleep(1 * time.Millisecond)
	}
}

// Checks the matcher and handler and gets the event channel from the group.
func (b *EventBus) channel(m eh.EventMatcher, h eh.EventHandler, observer bool) <-chan evt {
	b.registeredMu.Lock()
	defer b.registeredMu.Unlock()

	if m == nil {
		panic("matcher can't be nil")
	}
	if h == nil {
		panic("handler can't be nil")
	}
	if _, ok := b.registered[h.HandlerType()]; ok {
		panic(fmt.Sprintf("multiple registrations for %s", h.HandlerType()))
	}
	b.registered[h.HandlerType()] = struct{}{}

	id := string(h.HandlerType())
	if observer { // Generate unique ID for each observer.
		id = fmt.Sprintf("%s-%s", id, uuid.New())
	}
	return b.group.channel(id)
}

// Close all the channels in the events bus group
func (b *EventBus) Close() {
	b.group.Close()
}

// Wait for all channels to close in the event bus group
func (b *EventBus) Wait() {
	b.wg.Wait()
}

// Group is a publishing group shared by multiple event busses locally, if needed.
type Group struct {
	bus             map[string]chan evt
	busMu           sync.RWMutex
	pendingMessages *PendingMessages
}

// NewGroup creates a Group.
func NewGroup(pendingMessages *PendingMessages) *Group {
	return &Group{
		pendingMessages: pendingMessages,
		bus:             map[string]chan evt{},
	}
}

type evt struct {
	ctx   context.Context
	event eh.Event
}

func (g *Group) channel(id string) <-chan evt {
	g.busMu.Lock()
	defer g.busMu.Unlock()

	if ch, ok := g.bus[id]; ok {
		return ch
	}

	ch := make(chan evt, DefaultQueueSize)
	g.bus[id] = ch
	return ch
}

func (g *Group) publish(ctx context.Context, event eh.Event) {
	g.pendingMessages.Lock()
	defer g.pendingMessages.Unlock()
	g.busMu.RLock()
	defer g.busMu.RUnlock()

	for _, ch := range g.bus {
		g.pendingMessages.Inc()
		select {
		case ch <- evt{ctx, event}:
		default:
			log.Fatal("ERROR: Waitablebus queue is full. Please increase DefaultQueueSize in yesgocommon/waitablebus/eventbus.go.")
			g.pendingMessages.Dec()
		}
	}
}

// Close all the open channels
func (g *Group) Close() {
	for _, ch := range g.bus {
		close(ch)
	}
	g.bus = nil
}

type PendingMessages struct {
	count int
	mu    sync.Mutex
}

func (p *PendingMessages) Inc() {
	p.count += 1
}

func (p *PendingMessages) Dec() {
	p.count -= 1
}

func (p *PendingMessages) IsEmpty() bool {
	return p.count == 0
}

func (p *PendingMessages) Lock() {
	p.mu.Lock()
}

func (p *PendingMessages) Unlock() {
	p.mu.Unlock()
}
