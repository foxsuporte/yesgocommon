package datetime

import (
	"testing"
)

func TestFormat(t *testing.T) {
	want := "2020-04-27T00:00:00-0300"

	inputDate := "2020-04-27T00:00:00-0300"
	got, err := Format(inputDate)

	if err != nil {
		t.Fatal("Error format date")
	}

	if got != want {
		t.Fatalf("Conversion invalid got: %v, want: %v", got, want)
	}
}
