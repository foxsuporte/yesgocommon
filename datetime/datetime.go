package datetime

import (
	"time"
)

const (
	DATELAYOUT = "2006-01-02T15:04:05-0700"
)

func Format(inputDate string) (string, error) {
	validDate, err := validate(inputDate)
	if err != nil {
		return inputDate, err
	}

	return validDate.Format(DATELAYOUT), nil
}

func Now() time.Time {
	return time.Now()
}

func NowFormated() string {
	return Now().Format(DATELAYOUT)
}

func FormatTimestamp(inputTimestamp time.Time) string {
	return inputTimestamp.Format(DATELAYOUT)
}

func validate(inputDate string) (time.Time, error) {
	parsedDate, err := parseDateTime(inputDate)
	return parsedDate, err
}

func parseDateTime(inputDate string) (time.Time, error) {
	newDatetime, err := time.Parse(DATELAYOUT, inputDate)
	return newDatetime, err
}
