package http

import (
	"testing"
)

func Test(t *testing.T) {
	r := NewHttpResponse(999, "CONTENTS")
	if r.GetStatusCode() != 999 || r.GetBody() != "CONTENTS" {
		t.Error()
	}
}

func TestJson(t *testing.T) {
	r := NewJsonHttpResponse(999, map[string]interface{}{"success": true})
	if r.GetStatusCode() != 999 || r.GetBody() != `{"success":true}` {
		t.Error(r.GetBody())
	}
}
