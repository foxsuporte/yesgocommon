package http

import (
	"encoding/json"
)

type HttpResponseInterface interface {
	GetStatusCode() int
	GetBody() string
	GetRequestID() string
}

type HttpResponse struct {
	statusCode int
	body       string
	requestID  string
}

func NewHttpResponse(statusCode int, body string) *HttpResponse {
	return &HttpResponse{statusCode, body, ""}
}

func NewJsonHttpResponse(statusCode int, contents interface{}) *HttpResponse {
	body, _ := json.Marshal(contents)
	return &HttpResponse{statusCode, string(body), ""}
}

func NewJsonHttpResponseWithRequestID(requestID string, statusCode int, contents interface{}) *HttpResponse {
	body, _ := json.Marshal(contents)
	return &HttpResponse{statusCode, string(body), requestID}
}

func (h *HttpResponse) GetStatusCode() int {
	return h.statusCode
}

func (h *HttpResponse) GetBody() string {
	return h.body
}

func (h *HttpResponse) GetRequestID() string {
	return h.requestID
}
