package queuesubscriber

import (
	"fmt"
	"testing"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type SqsMock struct {
	simulatedError    awserr.Error
	simulatedMessages []*sqs.Message
}

func (s SqsMock) ReceiveMessage(*sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	fmt.Println("receiving msg")
	if s.simulatedError != nil {
		return nil, s.simulatedError
	}
	if len(s.simulatedMessages) > 0 {
		return &sqs.ReceiveMessageOutput{
			Messages: s.simulatedMessages,
		}, nil
	}
	return nil, awserr.New("MOCK_ERROR", "Mock was not setup with any messages or errors", nil)
}

func (s SqsMock) DeleteMessage(*sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return &sqs.DeleteMessageOutput{}, nil
}

func TestPackage(t *testing.T) {

	var fatalCount int
	// Do not throw fatal errors while testing
	logFatalf = func(msg string, i ...interface{}) {
		fatalCount++
	}

	q := QueueSubscriber{}

	// Test init code
	q.StartQueueSubscriber("", func(string, func()) {})

	var s SqsMock

	// Test for queue does not exist
	s = SqsMock{
		simulatedError: awserr.New(sqs.ErrCodeQueueDoesNotExist, "", nil),
	}
	sqsNew = func(client.ConfigProvider) SqsInterface { return s }
	receiveAndProcessMessages("", s, func(string, func()) {})
	if fatalCount != 1 {
		t.Error("Queue does not exist should call log.Fatalf")
	}

	// Test for other errors
	s = SqsMock{
		simulatedError: awserr.New("OTHER_ERROR", "", nil),
	}
	sqsNew = func(client.ConfigProvider) SqsInterface { return s }
	receiveAndProcessMessages("", s, func(string, func()) {})

	msg := "Hello, world!"

	// Test success
	s = SqsMock{
		simulatedMessages: []*sqs.Message{
			&sqs.Message{
				Body: &msg,
			},
		},
	}
	sqsNew = func(client.ConfigProvider) SqsInterface { return s }
	receiveAndProcessMessages("", s, func(msg string, callback func()) { callback() })

	// Test loop
	stopQueue := make(chan bool, 1)
	stopQueue <- true
	Loop("", s, func(string, func()) {}, stopQueue)
}
