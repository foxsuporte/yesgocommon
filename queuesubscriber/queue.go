package queuesubscriber

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const NUM_WORKERS = 10

type QueueSubscriberInterface interface {
	StartQueueSubscriber(sqsTopicUrl string, processMessageHandle func(string, func())) func()
}

type QueueSubscriber struct {
	AwsSecret   string
	AwsKey      string
	AwsRegion   string
	AwsEndpoint string
}

var sqsNew = SqsNew

type SqsInterface interface {
	ReceiveMessage(*sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error)
	DeleteMessage(*sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error)
}

func SqsNew(cfg client.ConfigProvider) SqsInterface {
	return sqs.New(cfg)
}

func (q *QueueSubscriber) getSqsClient() (SqsInterface, error) {
	awsSecret := q.AwsSecret
	awsKey := q.AwsKey
	awsRegion := q.AwsRegion

	// Initialize a session in us-west-2 that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials.
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(awsRegion),
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
		Endpoint:    aws.String(q.AwsEndpoint),
	})

	if err != nil {
		return nil, err
	}

	// Create a SQS service client.
	s := sqsNew(sess)
	return s, nil
}

var logFatalf = log.Fatalf

func receiveAndProcessMessages(sqsTopicUrl string, s SqsInterface, processMessageHandle func(string, func())) {
	timeout := int64(20)

	// Receive a message from the SQS queue with long polling enabled.
	result, err := s.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl: &sqsTopicUrl,
		AttributeNames: aws.StringSlice([]string{
			"SentTimestamp",
		}),
		MaxNumberOfMessages: aws.Int64(1),
		MessageAttributeNames: aws.StringSlice([]string{
			"All",
		}),
		WaitTimeSeconds: aws.Int64(timeout),
	})

	if err != nil {
		logFatalf("[SQS] Error receiving message from SQS queue: %s", err.Error())
		return
	}

	if len(result.Messages) > 0 {
		log.Printf("[SQS] %d new messages received.\n", len(result.Messages))
		for i := range result.Messages {
			processOneMessage(s, sqsTopicUrl, result.Messages[i], processMessageHandle)
		}
	}
}

func processOneMessage(s SqsInterface, sqsTopicUrl string, msg *sqs.Message, processMessageHandle func(string, func())) {
	successHandle := func() {
		log.Printf("[SQS] Deleting message from queue...\n")
		_, err := s.DeleteMessage(&sqs.DeleteMessageInput{
			QueueUrl:      &sqsTopicUrl,
			ReceiptHandle: msg.ReceiptHandle,
		})
		if err != nil {
			log.Printf("[SQS] Error deleting message from queue: %s\n", err.Error())
		}
		log.Println("[SQS] Message has been deleted from queue")
	}
	processMessageHandle(*msg.Body, successHandle)
}

func (q *QueueSubscriber) StartQueueSubscriber(
	sqsTopicUrl string,
	processMessageHandle func(string, func()),
) func() {
	log.Println("[SQS] Starting queue subscriber")
	s, _ := q.getSqsClient()
	stopQueue := make(chan bool)
	for i := 0; i < NUM_WORKERS; i++ {
		go Loop(sqsTopicUrl, s, processMessageHandle, stopQueue)
	}
	return func() {
		stopQueue <- true
	}
}

func Loop(sqsTopicUrl string, sqsClient SqsInterface, processMessageHandle func(string, func()), stopQueue chan bool) {
	for {
		select {
		case <-stopQueue:
			return
		default:
			receiveAndProcessMessages(sqsTopicUrl, sqsClient, processMessageHandle)
		}
	}
}
