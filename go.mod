module bitbucket.org/foxsuporte/yesgocommon

go 1.23.0

require (
	github.com/aws/aws-sdk-go v1.55.5
	github.com/globalsign/mgo v0.0.0-20180828104044-6f9f54af1356
	github.com/google/uuid v1.1.1
	github.com/looplab/eventhorizon v0.5.0
	github.com/stretchr/testify v1.9.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.5.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
