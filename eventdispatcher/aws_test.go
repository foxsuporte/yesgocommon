package eventdispatcher

import (
	"log"
	"testing"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/service/sns"
)

type SnsMock struct {
	simulatedError awserr.Error
}

func (s *SnsMock) Publish(*sns.PublishInput) (*sns.PublishOutput, error) {
	return nil, s.simulatedError
}

func Test(t *testing.T) {
	var e AwsSqsEventDispatcher

	// Test success
	CreateSession = func(client.ConfigProvider) SNSInterface {
		return &SnsMock{}
	}
	e = AwsSqsEventDispatcher{}
	err := e.Dispatch(map[string]interface{}{})
	if err != nil {
		log.Println(err.Error())
	}

	// Test error handling
	CreateSession = func(client.ConfigProvider) SNSInterface {
		return &SnsMock{
			awserr.New("TEST_ERROR", "TEST_ERROR", nil),
		}
	}
	e = AwsSqsEventDispatcher{}
	err = e.Dispatch(map[string]interface{}{})
	if err != nil {
		log.Println(err.Error())
	}
}
