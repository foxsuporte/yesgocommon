package eventdispatcher

type EventDispatcherInterface interface {
	Dispatch(map[string]interface{}) error
}
