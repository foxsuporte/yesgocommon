package eventdispatcher

import (
	"encoding/json"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/client"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sns"
)

type AwsSqsEventDispatcher struct {
	AwsKey      string
	AwsSecret   string
	AwsRegion   string
	AwsTopic    string
	AwsEndpoint string
}

type SNSInterface interface {
	Publish(*sns.PublishInput) (*sns.PublishOutput, error)
}

var CreateSession = CreateSNSSession

var SNSNew = sns.New

func CreateSNSSession(sess client.ConfigProvider) SNSInterface {
	return SNSNew(sess)
}

func (e AwsSqsEventDispatcher) Dispatch(data map[string]interface{}) error {

	msgBytes, err := json.Marshal(data)

	if err != nil {
		return err
	}

	msg := string(msgBytes)

	awsKey := e.AwsKey
	awsSecret := e.AwsSecret
	awsRegion := e.AwsRegion

	// Creating an AWS session
	sess, errAwsSession := session.NewSession(&aws.Config{
		Region:      aws.String(awsRegion),
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
		Endpoint:    aws.String(e.AwsEndpoint),
	})

	if errAwsSession != nil {
		fmt.Printf("Erro  AWS NewSession: %s\n", errAwsSession.Error())
		return errAwsSession
	}

	svc := CreateSession(sess)

	input := &sns.PublishInput{
		Message:  &msg,
		TopicArn: &e.AwsTopic,
	}

	_, err = svc.Publish(input)

	if err != nil {
		fmt.Printf("Erro publishing event to SNS: %s\n", err.Error())
		return err
	}

	return nil
}
