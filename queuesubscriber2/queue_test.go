package queuesubscriber

import (
	"errors"
	"log"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

type SqsServiceClientMock struct {
	sqsiface.SQSAPI
}

func (s SqsServiceClientMock) GetQueueUrl(gqui *sqs.GetQueueUrlInput) (*sqs.GetQueueUrlOutput, error) {
	return &sqs.GetQueueUrlOutput{}, nil
}
func (s SqsServiceClientMock) ReceiveMessage(rmi *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	receiveMessageOutput := &sqs.ReceiveMessageOutput{}

	msgs := []*sqs.Message{
		&sqs.Message{
			Body: aws.String(`{"field": "message 1"}`),
		},
	}
	receiveMessageOutput.SetMessages(msgs)
	return receiveMessageOutput, nil
}

func (s SqsServiceClientMock) DeleteMessage(dmi *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return &sqs.DeleteMessageOutput{}, nil
}

type SqsServiceClientMockReturnsError struct {
	sqsiface.SQSAPI
}

func (s SqsServiceClientMockReturnsError) GetQueueUrl(gqui *sqs.GetQueueUrlInput) (*sqs.GetQueueUrlOutput, error) {
	return nil, errors.New("Escape from loop")
}
func (s SqsServiceClientMockReturnsError) ReceiveMessage(rmi *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	return &sqs.ReceiveMessageOutput{}, nil
}

func (s SqsServiceClientMockReturnsError) DeleteMessage(dmi *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return &sqs.DeleteMessageOutput{}, nil
}

func TestNewSqsServiceClient(t *testing.T) {
	if _, err := NewSqsServiceClient("awsSecret", "awsKey", "awsRegion"); err != nil {
		t.Error()
	}
}

func TestDeleteMessageCallback(t *testing.T) {
	queueUrl := "queueUrl"
	receiptHandle := "receiptHandle"
	svc := SqsServiceClientMock{}
	deleteMessageCallback := DeleteMessageCallback(&queueUrl, &receiptHandle, svc)

	err := deleteMessageCallback()
	if err != nil {
		log.Println(err.Error())
	}
}

func TestReceiveMessage(t *testing.T) {
	resultURL := &sqs.GetQueueUrlOutput{}
	svc := SqsServiceClientMock{}
	publishChannel := make(chan SQSMessage, 2)
	err := receiveMessage(resultURL, svc, publishChannel)
	if err != nil {
		t.Error()
	}
}

func TestProcessMessage(t *testing.T) {
	msgBody := ""
	queueUrl := "fakeQueueUrl"
	receiptHandle := "fakeReceiptHandle"
	svc := SqsServiceClientMock{}
	publishChannel := make(chan SQSMessage, 2)
	processMessage(msgBody, &queueUrl, &receiptHandle, svc, publishChannel)
}

func TestNewStartPollingQueue(t *testing.T) {
	publishChannel := make(chan SQSMessage, 2)
	startPollingQueue := NewStartPollingQueue(
		"fakeAwsQueueName",
		SqsServiceClientMockReturnsError{},
		publishChannel,
	)
	startPollingQueue()
}
