package queuesubscriber

import (
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/service/sqs/sqsiface"
)

type SQSMessage struct {
	Body      string
	OnSuccess func() error
	OnError   func(error)
}

type EventMessage struct {
	EventType string `json:"event_type"`
	Body      string `json:"body"`
}

type QueueSubscriber struct {
	AwsQueueName   string
	AwsSecret      string
	AwsKey         string
	AwsRegion      string
	PublishChannel chan SQSMessage
}

func NewSqsServiceClient(awsSecret, awsKey, awsRegion string) (sqsiface.SQSAPI, error) {
	// Initialize a session in us-west-2 that the SDK will use to load
	// credentials from the shared credentials file ~/.aws/credentials.
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(awsRegion),
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
	})

	if err != nil {
		return nil, err
	}

	// Create a SQS service client.
	svc := sqs.New(sess)
	return svc, nil

}

func NewStartPollingQueue(awsQueueName string, svc sqsiface.SQSAPI, publishChannel chan SQSMessage) func() {
	return func() {
		// Need to convert the queue name into a URL. Make the GetQueueUrl
		// API call to retrieve the URL. This is needed for receiving messages
		// from the queue.
		resultURL, err := svc.GetQueueUrl(&sqs.GetQueueUrlInput{
			QueueName: aws.String(awsQueueName),
		})

		if err != nil {
			log.Println("[QueueSubscriber2] Erro ao inicializar fila: ", err)
			return
		}

		//Receive message
		for {
			err := receiveMessage(resultURL, svc, publishChannel)
			if err != nil {
				log.Println(err.Error())
			}
		}
	}
}

func receiveMessage(resultURL *sqs.GetQueueUrlOutput, svc sqsiface.SQSAPI, publishChannel chan SQSMessage) error {
	log.Println("[QueueSubscriber2] Recebendo mensagem SQS...")
	timeout := int64(20)

	// Receive a message from the SQS queue with long polling enabled.
	result, err := svc.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl: resultURL.QueueUrl,
		AttributeNames: aws.StringSlice([]string{
			"SentTimestamp",
		}),
		MaxNumberOfMessages: aws.Int64(10),
		MessageAttributeNames: aws.StringSlice([]string{
			"All",
		}),
		WaitTimeSeconds: aws.Int64(timeout),
	})

	if err != nil {
		log.Println("[QueueSubscriber2] Erro ao receber mensagem")
		return err
	}

	log.Println("[QueueSubscriber2] Recebidas ", len(result.Messages), " novas mensagens.")
	if len(result.Messages) > 0 {
		for i := range result.Messages {
			log.Println("[QueueSubscriber2] Processing: ", *result.Messages[i].Body)
			go processMessage(
				*result.Messages[i].Body,
				resultURL.QueueUrl,
				result.Messages[i].ReceiptHandle,
				svc,
				publishChannel,
			)
		}
	}
	return nil
}

func processMessage(msgBody string, queueUrl, receiptHandle *string, svc sqsiface.SQSAPI, publishChannel chan SQSMessage) {
	msg := SQSMessage{}
	msg.Body = msgBody
	msg.OnSuccess = DeleteMessageCallback(queueUrl, receiptHandle, svc)
	msg.OnError = func(err error) {
		log.Println("[QueueSubscriber2] Error processMessage: ", err)
	}
	publishChannel <- msg
}

func DeleteMessageCallback(queueUrl, receiptHandle *string, svc sqsiface.SQSAPI) func() error {
	return func() error {
		_, err := svc.DeleteMessage(&sqs.DeleteMessageInput{
			QueueUrl:      queueUrl,
			ReceiptHandle: receiptHandle,
		})
		if err != nil {
			return err
		}
		return nil
	}
}
