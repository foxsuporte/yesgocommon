package yesqueuesubscriber

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func NewCamundaHttpClient(workerID, camundaEndpoint string) CamundaHttpClient {
	return CamundaHttpClient{workerID, camundaEndpoint}
}

// CamundaHttpClient implements CamundaClient interface
type CamundaHttpClient struct {
	workerID        string
	camundaEndpoint string
}

type FetchAndLockPayload struct {
	WorkerId             string  `json:"workerId"`
	MaxTasks             int     `json:"maxTasks"`
	Topics               []Topic `json:"topics"`
	AsyncResponseTimeout int     `json:"asyncResponseTimeout"`
}
type StartProcessDefinitionPayload struct {
	Variables   map[string]ValueAndType `json:"variables"`
	BusinessKey string                  `json:"businessKey"`
}

type ValueAndType struct {
	Value string `json:"value"`
	Type  string `json:"type"`
}

type Topic struct {
	TopicName    string `json:"topicName"`
	LockDuration int    `json:"lockDuration"`
}

var httpPost = http.Post
var jsonMarshal = json.Marshal

// FetchAndLock fetches a task from Camunda's API and lock it for a period of time.
func (c CamundaHttpClient) FetchAndLock(payload FetchAndLockPayload) (Message, error) {
	payloadBytes, err := jsonMarshal(payload)
	if err != nil {
		return nil, err
	}

	resp, err := httpPost(
		c.CamundaEndpoint()+"/external-task/fetchAndLock",
		"application/json",
		bytes.NewReader(payloadBytes),
	)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusInternalServerError {
		return nil, RetriableError{http.StatusInternalServerError}
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var tasks []CamundaExternalTask
	err = json.Unmarshal(body, &tasks)
	if err != nil {
		return nil, err
	}

	if len(tasks) == 0 {
		return nil, nil
	}

	taskBody, err := jsonMarshal(tasks[0])
	if err != nil {
		return nil, err
	}

	msg := CamundaMessage{id: tasks[0].ID, body: string(taskBody)} // ID = task ID
	return msg, nil
}

type HandlerExternalTaskFailPayLoad struct {
	TaskID         string                 `json:"-"`
	WorkerId       string                 `json:"workerId"`
	ErrorDetails   string                 `json:"errorDetails"`
	ErrorMessage   string                 `json:"errorMessage"`
	Retries        int                    `json:"retries"`
	RetryTimeout   int64                  `json:"retryTimeout"`
	Variables      map[string]interface{} `json:"variables"`
	LocalVariables map[string]interface{} `json:"localVariables"`
}

// HandlerExternalTaskFail returns to camunda an error, Reports a failure to execute an external task by id.
// A number of retries and a timeout until the task can be retried can be specified. If retries are set to 0, an incident for this task is created.
// https://docs.camunda.org/manual/7.15/reference/rest/external-task/post-failure/
func (c CamundaHttpClient) HandlerExternalTaskFail(payload HandlerExternalTaskFailPayLoad) error {
	payloadBytes, err := jsonMarshal(payload)
	if err != nil {
		return err
	}
	resp, err := httpPost(
		c.camundaEndpoint+"/external-task/"+payload.TaskID+"/failure",
		"application/json",
		bytes.NewReader(payloadBytes),
	)
	if err != nil {
		return err
	}
	if resp.StatusCode != 204 {
		return fmt.Errorf(
			"[camundahttpclient.go] [CamundaHttpClient] HandlerExternalTaskFail returned http status: %v. Error: %v",
			resp.StatusCode,
			NewCamundaErrorFromResponse(resp),
		)
	}
	return nil
}

type CompleteTaskPayload struct {
	TaskID         string                 `json:"-"`
	WorkerId       string                 `json:"workerId"`
	Variables      map[string]interface{} `json:"variables"`
	LocalVariables map[string]interface{} `json:"localVariables"`
}

// CompleteTask completes a Camunda task
func (c CamundaHttpClient) CompleteTask(payload CompleteTaskPayload) error {

	payloadBytes, err := jsonMarshal(payload)
	if err != nil {
		return err
	}
	resp, err := httpPost(
		c.camundaEndpoint+"/external-task/"+payload.TaskID+"/complete",
		"application/json",
		bytes.NewReader(payloadBytes),
	)
	if err != nil {
		return err
	}
	if resp.StatusCode != 204 {
		return fmt.Errorf(
			"[camundahttpclient.go] [CamundaHttpClient] CompleteTask returned http status: %v. Error: %v",
			resp.StatusCode,
			NewCamundaErrorFromResponse(resp),
		)
	}
	return nil
}

func NewCamundaErrorFromResponse(resp *http.Response) error {
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	var camundaError CamundaError
	err = json.Unmarshal(body, &camundaError)
	if err != nil {
		return err
	}
	return camundaError
}

type CamundaError struct {
	Type    string
	Message string
}

func (e CamundaError) Error() string {
	return fmt.Sprintf("CamundaError: [Type: %v] [Message: %v]", e.Type, e.Message)
}

func (c CamundaHttpClient) CamundaEndpoint() string { return c.camundaEndpoint }

func (c CamundaHttpClient) WorkerID() string { return c.workerID }

func (c CamundaHttpClient) StartProcessDefinition(
	processDefinitionKey string,
	payload StartProcessDefinitionPayload,
) (string, error) {
	payloadBytes, err := jsonMarshal(payload)
	if err != nil {
		return "", err
	}
	pdkey := url.PathEscape(processDefinitionKey)
	resp, err := httpPost(
		c.camundaEndpoint+"/process-definition/key/"+pdkey+"/start",
		"application/json",
		bytes.NewReader(payloadBytes),
	)
	if err != nil {
		return "", err
	}

	if resp.StatusCode != 200 {
		return "", fmt.Errorf(
			"[camundahttpclient.go] [CamundaHttpClient] StartProcessDefinition: POST got StatusCode: %v. Error: %v",
			resp.StatusCode,
			NewCamundaErrorFromResponse(resp),
		)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	var bodyMap map[string]interface{}
	err = json.Unmarshal(body, &bodyMap)
	if err != nil {
		return "", err
	}
	if processInstanceId, ok := bodyMap["id"].(string); ok {
		return processInstanceId, nil
	}
	return "", errors.New("[camundahttpclient.go] [CamundaHttpClient] StartProcessDefinition: Could not find 'id' field in Camunda Response")
}

// CamundaExternalTask is the response got from Camunda API when fetching a task
type CamundaExternalTask struct {
	ActivityID           string                 `json:"activityId"`
	ActivityInstanceID   string                 `json:"activityInstanceId"`
	ErrorMessage         string                 `json:"errorMessage"`
	ErrorDetails         string                 `json:"errorDetails"`
	ExecutionID          string                 `json:"executionId"`
	ID                   string                 `json:"id"`
	LockExpirationTime   string                 `json:"lockExpirationTime"`
	ProcessDefinitionID  string                 `json:"processDefinitionId"`
	ProcessDefinitionKey string                 `json:"processDefinitionKey"`
	ProcessInstanceID    string                 `json:"processInstanceId"`
	TenantID             string                 `json:"tenantId"`
	Retries              float64                `json:"retries"`
	WorkerID             string                 `json:"workerId"`
	Priority             float64                `json:"priority"`
	TopicName            string                 `json:"topicName"`
	BusinessKey          string                 `json:"businessKey"`
	Variables            map[string]interface{} `json:"variables"`
}
