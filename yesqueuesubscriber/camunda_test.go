package yesqueuesubscriber

import (
	"errors"
	"log"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type CamundaClientMock struct {
	mock.Mock
}

func (m *CamundaClientMock) FetchAndLock(FetchAndLockPayload) (Message, error) {
	log.Println("[camunda_test.go] [CamundaClientMock] FetchAndLock")
	args := m.Called()
	return args.Get(0).(CamundaMessage), args.Error(1)
}
func (m *CamundaClientMock) CompleteTask(CompleteTaskPayload) error {
	log.Println("[camunda_test.go] [CamundaClientMock] CompleteTask")
	args := m.Called()
	return args.Error(0)
}
func (m *CamundaClientMock) CamundaEndpoint() string {
	log.Println("[camunda_test.go] [CamundaClientMock] CamundaEndpoit")
	args := m.Called()

	return args.String(0)
}
func (m *CamundaClientMock) WorkerID() string {
	log.Println("[camunda_test.go] [CamundaClientMock] WorkerID")
	args := m.Called()
	return args.String(0)
}
func (m *CamundaClientMock) StartProcessDefinition(string, StartProcessDefinitionPayload) (string, error) {
	log.Println("[camunda_test.go] [CamundaClientMock] WorkerID")
	args := m.Called()
	return args.String(0), args.Error(1)
}
func (m *CamundaClientMock) HandlerExternalTaskFail(payload HandlerExternalTaskFailPayLoad) error {
	log.Println("[camunda_test.go] [CamundaClientMock] Handler")
	args := m.Called()
	return args.Error(1)
}

// go test -v -run ^TestCamundaQueue$
func TestCamundaQueue(t *testing.T) {
	// shared vars
	topicName := "cw-testing-camunda-queue"
	lockDuration := 300 // in milliseconds
	longPollingDuration := 5000

	t.Run("HappyPath", func(t *testing.T) {
		// create camunda client
		camundaClientMock := new(CamundaClientMock)

		// setup mock's expectations for success case
		camundaClientMock.On("WorkerID").Return("mock-worker-id")
		camundaClientMock.On("FetchAndLock").Return(
			CamundaMessage{id: "camunda-task-id", body: `{"field":"value"}`},
			nil,
		)
		camundaClientMock.On("CompleteTask").Return(nil)

		// create camunda queue
		camundaQueue := NewCamundaQueue(camundaClientMock, topicName, lockDuration, longPollingDuration)

		messageHandler := func(msg string, success, retry, fail func()) {
			log.Printf("Handling a brand new Camunda task with body: %v", msg)
			success()
		}

		// receive and process one message
		receiveAndProcessMessages(camundaQueue, messageHandler)
		camundaClientMock.AssertExpectations(t)
	})

	t.Run("Fail", func(t *testing.T) {
		t.Run("FetchAndLock", func(t *testing.T) {
			logFatalf = func(s string, args ...interface{}) {
				log.Printf(s, args)
			}
			defer func() { logFatalf = log.Fatalf }()
			// create camunda client
			camundaClientMock := new(CamundaClientMock)

			// setup mock's expectations
			camundaClientMock.On("WorkerID").Return("mock-worker-id")
			camundaClientMock.On("FetchAndLock").Return(
				CamundaMessage{},
				errors.New("TestError - Error at fetching task from Camunda"),
			)

			// create camunda queue
			camundaQueue := NewCamundaQueue(camundaClientMock, topicName, lockDuration, longPollingDuration)

			messageHandler := func(msg string, s, r, f func()) {
				log.Println("[camunda_test.go] [Fail] [FetchAndLock]")
			}
			// receive and process one message
			receiveAndProcessMessages(camundaQueue, messageHandler)
			camundaClientMock.AssertExpectations(t)
		})

		t.Run("ProcessingMessage", func(t *testing.T) {
			// create camunda client
			camundaClientMock := new(CamundaClientMock)

			// setup mock's expectations
			camundaClientMock.On("WorkerID").Return("mock-worker-id")
			camundaClientMock.On("FetchAndLock").Return(CamundaMessage{}, nil)

			// create camunda queue
			camundaQueue := NewCamundaQueue(camundaClientMock, topicName, lockDuration, longPollingDuration)

			messageHandler := func(msg string, s, r, f func()) {
				log.Println("[camunda_test.go] [Fail] [ProcessingMesage] Calling Fail() function")
				f()
			}
			// receive and process one message
			receiveAndProcessMessages(camundaQueue, messageHandler)
			camundaClientMock.AssertExpectations(t)
		})

	})

	t.Run("SendMessage", func(t *testing.T) {
		// create camunda client
		camundaClientMock := new(CamundaClientMock)

		// create camunda queue
		camundaQueue := NewCamundaQueue(camundaClientMock, topicName, lockDuration, longPollingDuration)

		// create camunda message
		msg := CamundaMessage{id: "", body: `{"field":"value"}`}

		err := camundaQueue.SendMessage(msg)
		assert.Nil(t, err, "should not throw any error when sending message to camunda")
	})
}
