package yesqueuesubscriber

import (
	"log"
	"os"
	"testing"
	"time"
)

type QueueMock struct {
}

func (m QueueMock) ReceiveMessage() (Message, error) { return nil, nil }
func (m QueueMock) DeleteMessage(Message) error      { return nil }
func (m QueueMock) SendMessage(Message) error        { return nil }
func (m QueueMock) GetSuccessFunc(Message) func()    { return func() {} }
func (m QueueMock) GetFailFunc(Message) func()       { return func() {} }
func (m QueueMock) GetRetryFunc(Message) func()      { return func() {} }

func TestStartQueueSubscriber(t *testing.T) {
	queueMock := QueueMock{}
	messageHandle := func(msg string, success, retry, fail func()) {}
	stopFunc := StartQueueSubscriber(queueMock, messageHandle)
	time.Sleep(time.Millisecond * 1)
	stopFunc()
}

func ExampleSQS() {
	os.Setenv("AWS_KEY", "key")
	os.Setenv("AWS_SECRET", "secret")
	os.Setenv("SQS_TOPIC_URL", "topic-url")

	awsKey := os.Getenv("AWS_KEY")
	awsSecret := os.Getenv("AWS_SECRET")
	sqsClient, err := NewSQSClient("us-east-1", awsKey, awsSecret)
	if err != nil {
		panic(err)
	}

	sqsTopic := os.Getenv("SQS_TOPIC_URL")
	queue := NewSQSQueue(sqsClient, sqsTopic, int64(20))

	handler := func(msg string, success, retry, fail func()) {
		log.Println("Processei uma mensagem")
		success()
	}
	stopFunc := StartQueueSubscriber(queue, handler)
	time.Sleep(time.Millisecond * 1)
	stopFunc()
}

/*
func ExampleCamunda() {
	os.Setenv("CAMUNDA_ENDPOINT", "endpoint")

	workerID := uuid.New()
	camundaEndpoint := os.Getenv("CAMUNDA_ENDPOINT")
	camundaClient := NewCamundaHttpClient(workerID.String(), camundaEndpoint)

	queue := NewCamundaQueue(camundaClient, "cw-print", 5000, 20000)

	messageHandler := func(msg string, s, r, f func()) {
		log.Printf("[QueueSubscriber] [Camunda] Received message: %v", msg)
		var task CamundaExternalTask
		err := json.Unmarshal([]byte(msg), &task)
		if err != nil {
			log.Fatal(err)
		}
		log.Printf("Printing text: %v", task.Variables["printText"].(map[string]interface{})["value"])
		// call success handle and complete task in camunda
		s()
	}
	StartQueueSubscriber(queue, messageHandler)
}*/
