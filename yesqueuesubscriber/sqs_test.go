package yesqueuesubscriber

import (
	"errors"
	"log"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestNewSQSClient(t *testing.T) {
	t.Run("EmptyConfig", func(t *testing.T) {
		_, err := NewSQSClient("us-east-1", "", "aws-super-secret")
		assert.Error(t, err, "should throw error when trying to create a new sqs client without configs")
	})

	t.Run("WithoutError", func(t *testing.T) {
		_, err := NewSQSClient("us-east-1", "aws-key", "aws-super-secret")
		assert.Nil(t, err, "should not throw error when trying to create")
	})

	t.Run("WithError", func(t *testing.T) {
		newAwsSession = func(...*aws.Config) (*session.Session, error) {
			return &session.Session{}, errors.New("Test error")
		}
		_, err := NewSQSClient("aws-region", "aws-key", "aws-secret")
		assert.Error(t, err, "should throw error when it was not able to create aws session")
		newAwsSession = session.NewSession
	})
}

// When successfully handling, queue subscriber with sqs should call ReceiveMessage
// and then DeleteMessage in sqs client
func TestQueueSubscriberWithSQSSuccess(t *testing.T) {

	t.Run("Success", func(t *testing.T) {
		// create sqs client mock and setup expectations
		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					ReceiptHandle: aws.String("receipt-handle"),
					Body:          aws.String(`{"field": "value"}`),
				},
			},
		}, nil)
		sqsClientMock.On("DeleteMessage").Return(&sqs.DeleteMessageOutput{}, nil)

		// create queue using mocked sqs client
		queue := NewSQSQueue(sqsClientMock, "topic", 20)

		// create a variable so we can check if the handleMessage function was called
		wasHandled := false
		handleMessage := func(msg string, success, retry, fail func()) {
			log.Println("Handling Message")
			wasHandled = true
			success()
		}

		// run
		receiveAndProcessMessages(queue, handleMessage)

		// assertions
		assert.True(t, wasHandled, "should call handleMessageFunc when receiving message")
		sqsClientMock.AssertExpectations(t)
	})

	t.Run("NoMessages", func(t *testing.T) {
		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		// create sqs client mock and setup expectations
		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(
			&sqs.ReceiveMessageOutput{},
			nil,
		)
		// create queue using mocked sqs client
		queue := NewSQSQueue(sqsClientMock, "topic", 20)

		// create a variable so we can check if the handleMessage function was called
		handleMessage := func(msg string, success, retry, fail func()) {}

		// run
		receiveAndProcessMessages(queue, handleMessage)

		sqsClientMock.AssertExpectations(t)
	})

	t.Run("ErrorWhenReceivingMessage", func(t *testing.T) {

		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		// create sqs client mock and setup expectations
		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(
			&sqs.ReceiveMessageOutput{},
			awserr.New("OTHER_ERROR", "[TEST] Error communicating with AWS", nil),
		)
		// create queue using mocked sqs client
		queue := NewSQSQueue(sqsClientMock, "topic", 20)

		// create a variable so we can check if the handleMessage function was called
		wasHandled := false
		handleMessage := func(msg string, success, retry, fail func()) {
			log.Println("Handling Message")
			wasHandled = true
			success()
		}

		// run
		receiveAndProcessMessages(queue, handleMessage)

		// assertions
		assert.False(t, wasHandled, "should not call handleMessageFunc if receiving message returns error")
		sqsClientMock.AssertExpectations(t)
	})

	// in this case, we're going to just ignore, because we are assuming that the message handle
	// is idempotent and when the same message come (and it will because of aws sqs), the handler
	// won't process it and instead, delete it.
	t.Run("ErrorWhenDeletingMessageAfterSuccessfullyHandling", func(t *testing.T) {

		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					Body:          aws.String(`{"field": "value"}`),
					ReceiptHandle: aws.String(`receipt-handle`),
				},
			},
		}, nil)
		sqsClientMock.On("DeleteMessage").Return(
			&sqs.DeleteMessageOutput{},
			awserr.New("TEST", "Failed at deleting message from queue", nil),
		)
		queue := NewSQSQueue(sqsClientMock, "", int64(5))
		messageHandler := func(msg string, s, r, f func()) {
			s()
		}

		receiveAndProcessMessages(queue, messageHandler)
		sqsClientMock.AssertExpectations(t)
	})

	t.Run("RetryingSuccessful", func(t *testing.T) {
		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					Body:          aws.String(`{"field": "value"}`),
					ReceiptHandle: aws.String(`receipt-handle`),
				},
			},
		}, nil)
		sqsClientMock.On("SendMessage").Return(
			&sqs.SendMessageOutput{},
			nil,
		)
		sqsClientMock.On("DeleteMessage").Return(
			&sqs.DeleteMessageOutput{},
			nil,
		)
		queue := NewSQSQueue(sqsClientMock, "", int64(5))
		messageHandler := func(msg string, s, r, f func()) {
			r()
		}
		receiveAndProcessMessages(queue, messageHandler)
		sqsClientMock.AssertExpectations(t)
	})

	t.Run("RetryingAndFailAtDeletingOldMessage", func(t *testing.T) {
		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					Body:          aws.String(`{"field": "value"}`),
					ReceiptHandle: aws.String(`receipt-handle`),
				},
			},
		}, nil)
		sqsClientMock.On("SendMessage").Return(
			&sqs.SendMessageOutput{},
			nil,
		)
		sqsClientMock.On("DeleteMessage").Return(
			&sqs.DeleteMessageOutput{},
			awserr.New("TEST", "[Retry] Failed at deleting old message", nil),
		)
		queue := NewSQSQueue(sqsClientMock, "", int64(5))
		messageHandler := func(msg string, s, r, f func()) {
			r()
		}
		receiveAndProcessMessages(queue, messageHandler)
		sqsClientMock.AssertExpectations(t)
	})

	t.Run("RetryingFailAtSendingNewMessage", func(t *testing.T) {

		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					Body:          aws.String(`{"field": "value"}`),
					ReceiptHandle: aws.String(`receipt-handle`),
				},
			},
		}, nil)
		sqsClientMock.On("SendMessage").Return(
			&sqs.SendMessageOutput{},
			awserr.New("TEST", "[Retry] Failed at sending new message", nil),
		)
		queue := NewSQSQueue(sqsClientMock, "", int64(5))
		messageHandler := func(msg string, s, r, f func()) {
			r()
		}
		receiveAndProcessMessages(queue, messageHandler)
		sqsClientMock.AssertExpectations(t)
	})

	t.Run("Fail", func(t *testing.T) {
		logFatalf = func(s string, args ...interface{}) {
			log.Printf(s, args)
		}
		defer func() { logFatalf = log.Fatalf }()

		sqsClientMock := new(SQSClientMock)
		sqsClientMock.On("ReceiveMessage").Return(&sqs.ReceiveMessageOutput{
			Messages: []*sqs.Message{
				&sqs.Message{
					Body:          aws.String(`{"field": "value"}`),
					ReceiptHandle: aws.String(`receipt-handle`),
				},
			},
		}, nil)
		queue := NewSQSQueue(sqsClientMock, "", int64(5))
		sqsClientMock.On("DeleteMessage").Return(
			&sqs.DeleteMessageOutput{},
			nil,
		)
		messageHandler := func(msg string, s, r, f func()) {
			f()
		}
		receiveAndProcessMessages(queue, messageHandler)
		sqsClientMock.AssertExpectations(t)
	})
}

type SQSClientMock struct {
	mock.Mock
}

func (m *SQSClientMock) ReceiveMessage(*sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	log.Println("[queue_test.go] [SQSClientMock] ReceiveMessage")
	args := m.Called()
	return args.Get(0).(*sqs.ReceiveMessageOutput), args.Error(1)
}
func (m *SQSClientMock) DeleteMessage(*sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	log.Println("[queue_test.go] [SQSClientMock] DeleteMessage")
	args := m.Called()
	return args.Get(0).(*sqs.DeleteMessageOutput), args.Error(1)
}
func (m *SQSClientMock) SendMessage(*sqs.SendMessageInput) (*sqs.SendMessageOutput, error) {
	log.Println("[queue_test.go] [SQSClientMock] SendMessage")
	args := m.Called()
	return args.Get(0).(*sqs.SendMessageOutput), args.Error(1)
}
