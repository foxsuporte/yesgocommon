package yesqueuesubscriber

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

// go test -v -run ^TestCamundaHttpClient$
func TestCamundaHttpClient(t *testing.T) {
	workerID := "worker-id"
	endpoint := "http://camunda-endpoint/engine-rest"
	client := NewCamundaHttpClient(workerID, endpoint)

	payload := FetchAndLockPayload{
		MaxTasks: 1,
		Topics:   []Topic{Topic{"topic-name", 1000}}, AsyncResponseTimeout: 100,
	}

	t.Run("FetchAndLock", func(t *testing.T) {
		t.Run("Success", func(t *testing.T) {
			t.Run("OneMessage", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`[{"id":"taskId"}]`)))
					return &http.Response{Body: body}, nil
				}
				msg, err := client.FetchAndLock(payload)
				assert.NotNil(t, msg, "Should have message")
				assert.Nil(t, err, "Should not return error")
				httpPost = http.Post
			})
			t.Run("NoMessages", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`[]`)))
					return &http.Response{Body: body}, nil
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not have any messages")
				assert.Nil(t, err, "Should not return error")
				httpPost = http.Post
			})
			t.Run("StatusCode500Message", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`{}`)))
					return &http.Response{Body: body, StatusCode: 500}, nil
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not have any messages")
				assert.Equal(t, RetriableError{HttpStatusCode: 500}, err, "Should return a RetriableError Struct with HttpStatusCode 500")
				httpPost = http.Post
			})
		})
		t.Run("Fail", func(t *testing.T) {
			t.Run("MarshallingPayload", func(t *testing.T) {
				errorExpected := errors.New("TEST - Error when marshalling payload")
				jsonMarshal = func(interface{}) ([]byte, error) {
					return []byte{}, errorExpected
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not have any messages")
				assert.ErrorIs(t, err, errorExpected, "Should return error when marshalling payload")
				jsonMarshal = json.Marshal
			})
			t.Run("Posting", func(t *testing.T) {
				errorExpected := errors.New("TEST - Error when posting")
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`[]`)))
					return &http.Response{Body: body}, errorExpected
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not have any messages")
				assert.ErrorIs(t, err, errorExpected, "Should return error")
				httpPost = http.Post
			})
			t.Run("UnmarshallingBody", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`[`)))
					return &http.Response{Body: body}, nil
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not have any messages")
				assert.NotNil(t, err, "Should return error")
				assert.Equal(t, "unexpected end of JSON input", err.Error())
				httpPost = http.Post
			})
			t.Run("MarshallingTask", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`[{"id":"taskId"}]`)))
					return &http.Response{Body: body}, nil
				}

				errorExpected := errors.New("TEST - Error when marshalling task")
				jsonMarshal = func(v interface{}) ([]byte, error) {
					if _, ok := v.(CamundaExternalTask); ok {
						return []byte{}, errorExpected
					}
					return []byte{}, nil
				}
				msg, err := client.FetchAndLock(payload)
				assert.Nil(t, msg, "Should not return any message when error")
				assert.ErrorIs(t, err, errorExpected, "Should return error when marshalling task")
				jsonMarshal = json.Marshal
			})
		})
	})
	t.Run("HandlerExternalTaskFail", func(t *testing.T) {
		t.Run("Sucess", func(t *testing.T) {
			httpPost = func(string, string, io.Reader) (*http.Response, error) {
				body := ioutil.NopCloser(bytes.NewReader([]byte(``)))
				return &http.Response{StatusCode: 204, Body: body}, nil
			}
			err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
			assert.Nil(t, err, "Should not return error when return a task fail")
			httpPost = http.Post
		})
		t.Run("Fail", func(t *testing.T) {
			t.Run("AtMarshallingPayload", func(t *testing.T) {
				errorExpected := errors.New("Test - Error when marshalling HandlerExternalTaskFailPayLoad")
				jsonMarshal = func(interface{}) ([]byte, error) {
					return []byte{}, errorExpected
				}
				err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
				assert.ErrorIs(t, err, errorExpected)
				jsonMarshal = json.Marshal
			})
			t.Run("AtPosting", func(t *testing.T) {
				t.Run("Error", func(t *testing.T) {
					errorExpected := errors.New("Test - Posting HandlerExternalTaskFail error")
					httpPost = func(string, string, io.Reader) (*http.Response, error) {
						body := ioutil.NopCloser(bytes.NewReader([]byte(``)))
						return &http.Response{StatusCode: 400, Body: body}, errorExpected
					}
					err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
					assert.ErrorIs(t, err, errorExpected)
				})
				t.Run("StatusCodeIsNotSuccess", func(t *testing.T) {
					t.Run("FailAtReadingErrorResponseBody", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							return &http.Response{StatusCode: 400, Body: FaultyReader{}}, nil
						}
						errorExpected := "[camundahttpclient.go] [CamundaHttpClient] HandlerExternalTaskFail returned http status: 400. Error: [FaultyReader] Test - Error at reading response body"
						err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
					t.Run("FailAtUnmarshallingErrorResponseBody", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							body := ioutil.NopCloser(bytes.NewReader([]byte(`[`)))
							return &http.Response{StatusCode: 400, Body: body}, nil
						}
						errorExpected := "[camundahttpclient.go] [CamundaHttpClient] HandlerExternalTaskFail returned http status: 400. Error: unexpected end of JSON input"
						err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
					t.Run("NoMoreErrors", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							body := ioutil.NopCloser(bytes.NewReader([]byte(`{"type": "SomeExceptionClass", "message": "Test error message"}`)))
							return &http.Response{StatusCode: 400, Body: body}, nil
						}
						errorExpected := `[camundahttpclient.go] [CamundaHttpClient] HandlerExternalTaskFail returned http status: 400. Error: CamundaError: [Type: SomeExceptionClass] [Message: Test error message]`
						err := client.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
				})
			})
		})

	})
	t.Run("CompleteTask_", func(t *testing.T) {
		t.Run("Success", func(t *testing.T) {
			httpPost = func(string, string, io.Reader) (*http.Response, error) {
				body := ioutil.NopCloser(bytes.NewReader([]byte(``)))
				return &http.Response{StatusCode: 204, Body: body}, nil
			}
			err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
			assert.Nil(t, err, "Should not return error when completing task")
			httpPost = http.Post
		})
		t.Run("Fail", func(t *testing.T) {
			t.Run("AtMarshallingPayload", func(t *testing.T) {
				errorExpected := errors.New("Test - Error when marshalling CompleteTaskPayload")
				jsonMarshal = func(interface{}) ([]byte, error) {
					return []byte{}, errorExpected
				}
				err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
				assert.ErrorIs(t, err, errorExpected)
				jsonMarshal = json.Marshal
			})
			t.Run("AtPosting", func(t *testing.T) {
				t.Run("Error", func(t *testing.T) {
					errorExpected := errors.New("Test - Posting CompleteTask error")
					httpPost = func(string, string, io.Reader) (*http.Response, error) {
						body := ioutil.NopCloser(bytes.NewReader([]byte(``)))
						return &http.Response{StatusCode: 400, Body: body}, errorExpected
					}
					err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
					assert.ErrorIs(t, err, errorExpected)
				})
				t.Run("StatusCodeIsNotSuccess", func(t *testing.T) {
					t.Run("FailAtReadingErrorResponseBody", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							return &http.Response{StatusCode: 400, Body: FaultyReader{}}, nil
						}
						errorExpected := "[camundahttpclient.go] [CamundaHttpClient] CompleteTask returned http status: 400. Error: [FaultyReader] Test - Error at reading response body"
						err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
					t.Run("FailAtUnmarshallingErrorResponseBody", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							body := ioutil.NopCloser(bytes.NewReader([]byte(`[`)))
							return &http.Response{StatusCode: 400, Body: body}, nil
						}
						errorExpected := "[camundahttpclient.go] [CamundaHttpClient] CompleteTask returned http status: 400. Error: unexpected end of JSON input"
						err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
					t.Run("NoMoreErrors", func(t *testing.T) {
						httpPost = func(string, string, io.Reader) (*http.Response, error) {
							body := ioutil.NopCloser(bytes.NewReader([]byte(`{"type": "SomeExceptionClass", "message": "Test error message"}`)))
							return &http.Response{StatusCode: 400, Body: body}, nil
						}
						errorExpected := `[camundahttpclient.go] [CamundaHttpClient] CompleteTask returned http status: 400. Error: CamundaError: [Type: SomeExceptionClass] [Message: Test error message]`
						err := client.CompleteTask(CompleteTaskPayload{TaskID: "taskId"})
						assert.EqualError(t, err, errorExpected)
					})
				})
			})
		})
	})

	t.Run("StartProcessDefinition", func(t *testing.T) {
		t.Run("Success", func(t *testing.T) {
			httpPost = func(string, string, io.Reader) (*http.Response, error) {
				body := ioutil.NopCloser(bytes.NewReader([]byte(`{"id": "new-process-instance"}`)))
				return &http.Response{StatusCode: 200, Body: body}, nil
			}
			payload := StartProcessDefinitionPayload{
				Variables: map[string]ValueAndType{
					"content": ValueAndType{`{"field":"value"}`, "string"},
				},
			}
			processInstanceId, err := client.StartProcessDefinition("process-definition-key", payload)
			assert.NotNil(t, processInstanceId, "Should return the id of the process instance created")
			assert.Nil(t, err, "Should not return error when starting a process definition")
		})
		t.Run("Fail", func(t *testing.T) {
			t.Run("MissingIDInResponseBody", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`{"field": "value"}`)))
					return &http.Response{StatusCode: 200, Body: body}, nil
				}
				payload := StartProcessDefinitionPayload{
					Variables: map[string]ValueAndType{
						"content": ValueAndType{`{"field":"value"}`, "string"},
					},
				}
				processInstanceId, err := client.StartProcessDefinition("process-definition-key", payload)
				assert.Empty(t, processInstanceId, "Should return empty id of the process instance created")
				errorExpected := `[camundahttpclient.go] [CamundaHttpClient] StartProcessDefinition: Could not find 'id' field in Camunda Response`
				assert.EqualError(t, err, errorExpected)
			})
			t.Run("AtMarshallingPayload", func(t *testing.T) {
				errorExpected := errors.New("Test - Error when marshalling StartProcessDefinitionPayload")
				jsonMarshal = func(interface{}) ([]byte, error) {
					return []byte{}, errorExpected
				}
				processInstanceId, err := client.StartProcessDefinition(
					"process-definition-key",
					StartProcessDefinitionPayload{},
				)
				assert.Equal(t, "", processInstanceId)
				assert.ErrorIs(t, err, errorExpected)
				jsonMarshal = json.Marshal

			})
			t.Run("AtPosting", func(t *testing.T) {
				t.Run("Request", func(t *testing.T) {
					errorExpected := errors.New("Test - Error when (POST) start process definition")
					httpPost = func(string, string, io.Reader) (*http.Response, error) {
						return &http.Response{}, errorExpected
					}
					processInstanceId, err := client.StartProcessDefinition(
						"process-definition-key",
						StartProcessDefinitionPayload{},
					)
					assert.Equal(t, "", processInstanceId)
					assert.ErrorIs(t, err, errorExpected)
				})
				t.Run("StatusCodeNotSuccess", func(t *testing.T) {
					httpPost = func(string, string, io.Reader) (*http.Response, error) {
						body := ioutil.NopCloser(bytes.NewReader([]byte(`{"type": "SomeExceptionClass", "message": "Test error message"}`)))
						return &http.Response{StatusCode: 400, Body: body}, nil
					}
					processInstanceId, err := client.StartProcessDefinition(
						"process-definition-key",
						StartProcessDefinitionPayload{},
					)
					assert.Equal(t, "", processInstanceId)
					expectedError := "[camundahttpclient.go] [CamundaHttpClient] StartProcessDefinition: POST got StatusCode: 400. Error: CamundaError: [Type: SomeExceptionClass] [Message: Test error message]"
					assert.EqualError(t, err, expectedError)
				})
			})
			t.Run("AtUnmarshallingResponseBody", func(t *testing.T) {
				httpPost = func(string, string, io.Reader) (*http.Response, error) {
					body := ioutil.NopCloser(bytes.NewReader([]byte(`{invalid-json}`)))
					return &http.Response{StatusCode: 200, Body: body}, nil
				}
				processInstanceId, err := client.StartProcessDefinition(
					"process-definition-key",
					StartProcessDefinitionPayload{},
				)
				assert.Equal(t, "", processInstanceId)
				assert.NotNil(t, err)
				httpPost = http.Post
			})
		})

	})
}

type FaultyReader struct {
	io.Reader
	io.Closer
}

func (FaultyReader) Read(p []byte) (n int, err error) {
	return 0, errors.New("[FaultyReader] Test - Error at reading response body")
}
