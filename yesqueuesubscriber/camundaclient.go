package yesqueuesubscriber

// CamundaClient is responsible for comunicating with Camunda REST API.
type CamundaClient interface {
	FetchAndLock(FetchAndLockPayload) (Message, error)
	CompleteTask(payload CompleteTaskPayload) error
	CamundaEndpoint() string
	WorkerID() string
	StartProcessDefinition(processDefinitionKey string, payload StartProcessDefinitionPayload) (processInstanceID string, err error)
	HandlerExternalTaskFail(payload HandlerExternalTaskFailPayLoad) error
	// TODO - BPMNError(taskID string) error
	// TODO - SetRetries(taskID string) error
	// TODO - Unlock(taskID string) error
}
