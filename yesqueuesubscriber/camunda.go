package yesqueuesubscriber

import (
	"log"
)

func NewCamundaQueue(
	camundaClient CamundaClient,
	topicName string,
	lockDuration int,
	longPollingTimeout int,
) CamundaQueue {
	return CamundaQueue{camundaClient, topicName, lockDuration, longPollingTimeout}
}

type CamundaQueue struct {
	camundaClient      CamundaClient
	topicName          string
	lockDuration       int // in milliseconds
	longPollingTimeout int // in milliseconds
}

type CamundaMessage struct {
	id   string
	body string
}

func (m CamundaMessage) ID() string {
	return m.id
}
func (m CamundaMessage) Body() string {
	return m.body
}
func (q CamundaQueue) ReceiveMessage() (Message, error) {
	log.Println("[camunda.go] [CamundaQueue.ReceiveMessage] Receiving message...")
	payload := FetchAndLockPayload{
		WorkerId: q.camundaClient.WorkerID(),
		MaxTasks: 1,
		Topics: []Topic{
			Topic{
				TopicName:    q.topicName,
				LockDuration: q.lockDuration,
			},
		},
		AsyncResponseTimeout: q.longPollingTimeout,
	}
	return q.camundaClient.FetchAndLock(payload)
}

// Complete camunda task
func (q CamundaQueue) DeleteMessage(msg Message) error {
	log.Printf("[camunda.go] [CamundaQueue.DeleteMessage] Completing task...\n")
	return q.camundaClient.CompleteTask(CompleteTaskPayload{TaskID: msg.ID()})
}

func (q CamundaQueue) SendMessage(Message) error {
	//TODO
	return nil
}
func (q CamundaQueue) GetSuccessFunc(msg Message) func() {
	return func() {
		err := q.DeleteMessage(msg)
		if err != nil {
			log.Printf("[camunda.go] [CamundaQueue] [SuccessFunc] Failed at completing task: %v", err)
		}
	}
}

func (q CamundaQueue) GetFailFunc(Message) func() {
	return func() {
		// TODO q.camundaClient.HandlerExternalTaskFail(HandlerExternalTaskFailPayLoad{})
		log.Printf("[camunda.go] [CamundaQueue] [FailFunc] Failed at processing Camunda task")
	}
}
func (q CamundaQueue) GetRetryFunc(Message) func() {
	//TODO
	return func() {}
}
