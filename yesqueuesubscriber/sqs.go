package yesqueuesubscriber

import (
	"errors"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
)

var newAwsSession = session.NewSession

func NewSQSClient(awsRegion, awsKey, awsSecret string) (SQSClient, error) {
	args := map[string]string{"awsRegion": awsRegion, "awsKey": awsKey, "awsSecret": awsSecret}
	for k, v := range args {
		if v == "" {
			return nil, errors.New("Variable " + k + " cannot be empty")
		}
	}
	sess, err := newAwsSession(&aws.Config{
		Region:      aws.String(awsRegion),
		Credentials: credentials.NewStaticCredentials(awsKey, awsSecret, ""),
	})
	if err != nil {
		return nil, err
	}
	return sqs.New(sess), nil
}

func NewSQSQueue(sqsClient SQSClient, sqsTopicUrl string, pollingTimeout int64) SQSQueue {
	return SQSQueue{sqsClient, sqsTopicUrl, pollingTimeout}
}

type SQSClient interface {
	ReceiveMessage(*sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error)
	DeleteMessage(*sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error)
	SendMessage(*sqs.SendMessageInput) (*sqs.SendMessageOutput, error)
}

type SQSQueue struct {
	sqsClient   SQSClient
	sqsTopicUrl string
	timeout     int64
}

type SQSMessage struct {
	receiptHandle string
	body          string
}

func (m SQSMessage) ID() string {
	return m.receiptHandle
}

func (m SQSMessage) Body() string {
	return m.body
}

func (q SQSQueue) ReceiveMessage() (Message, error) {
	log.Printf("[sqs.go] [SQSQueue.ReceiveMessage] Receiving message...\n")
	output, err := q.sqsClient.ReceiveMessage(&sqs.ReceiveMessageInput{
		QueueUrl: aws.String(q.sqsTopicUrl),
		AttributeNames: aws.StringSlice([]string{
			"SentTimestamp",
		}),
		MaxNumberOfMessages: aws.Int64(1),
		MessageAttributeNames: aws.StringSlice([]string{
			"All",
		}),
		WaitTimeSeconds: aws.Int64(q.timeout),
	})
	if err != nil {
		return nil, err
	}
	if len(output.Messages) == 0 {
		return nil, nil
	}
	msg := SQSMessage{
		receiptHandle: *output.Messages[0].ReceiptHandle,
		body:          *output.Messages[0].Body,
	}
	return msg, nil
}

func (q SQSQueue) DeleteMessage(msg Message) error {
	log.Printf("[sqs.go] [SQSQueue.DeleteMessage] [Message: Deleting message from queue...]\n")
	_, err := q.sqsClient.DeleteMessage(&sqs.DeleteMessageInput{
		QueueUrl:      aws.String(q.sqsTopicUrl),
		ReceiptHandle: aws.String(msg.ID()),
	})
	return err
}

func (q SQSQueue) SendMessage(msg Message) error {
	log.Printf("[sqs.go] [SQSQueue.SendMessage] [Message: Sending message to queue...]\n")
	_, err := q.sqsClient.SendMessage(&sqs.SendMessageInput{
		QueueUrl:    aws.String(q.sqsTopicUrl),
		MessageBody: aws.String(msg.Body()),
		//MessageAttributes: cfg.Msg.MessageAttributes,
	})
	return err
}

// Should be called when message handling need to be retried. In this case, we
// send a copy of the message to the queue and delete the former.
func (q SQSQueue) GetRetryFunc(msg Message) func() {
	return func() {
		log.Printf("[sqs.go] [RetryHandle] [Message: Retry message on queue...]\n")
		err := q.SendMessage(msg)
		if err == nil {
			err := q.DeleteMessage(msg)
			if err != nil {
				log.Printf("[sqs.go] [RetryHandle] Error deleting old message from queue...\n")
			}
		} else {
			log.Printf("[sqs.go] [RetryFunc] Error to retry message on queue...\n")
		}
	}
}

func (q SQSQueue) GetSuccessFunc(msg Message) func() {
	return func() {
		if err := q.DeleteMessage(msg); err != nil {
			logFatalf("sqs.go] [SuccessFunc] Failed at deleting message: *%v*", err)
		}
	}
}

func (q SQSQueue) GetFailFunc(msg Message) func() {
	return func() {
		log.Printf("[sqs.go] [FailFunc] [Message: Process message failed, not retriable, removing it...]\n")
		if err := q.DeleteMessage(msg); err != nil {
			logFatalf("sqs.go] [FailFunc] Failed at deleting message: *%v*", err)
		}
	}
}
