package yesqueuesubscriber

import (
	"fmt"
	"log"
)

const NUM_WORKERS = 10

type Queue interface {
	ReceiveMessage() (Message, error)
	DeleteMessage(Message) error
	SendMessage(Message) error
	GetSuccessFunc(Message) func()
	GetFailFunc(Message) func()
	GetRetryFunc(Message) func()
}

type Message interface {
	ID() string
	Body() string
}

type HandleMessageFunc func(msg string, success, retry, fail func())

var logFatalf = log.Fatalf

type RetriableError struct {
	HttpStatusCode int
}

func (e RetriableError) Error() string {
	return fmt.Sprintf("Retriable error, HttpStatusCode: %v", e.HttpStatusCode)
}

func receiveAndProcessMessages(queue Queue, handleMessage HandleMessageFunc) {
	// Receive a message from queue
	msg, err := queue.ReceiveMessage()

	if err != nil {
		if _, ok := err.(RetriableError); ok {
			log.Printf("[queue.go] [receiveAndProcessMessages] [Message: Retriable error at receiving message from queue: %s]\n", err.Error())
			return
		}
		logFatalf("[queue.go] [receiveAndProcessMessages] [Message: Error receiving message from queue: %s]\n", err.Error())
		return
	}

	if msg != nil {
		log.Printf("[queue.go] [receiveAndProcessMessages] Message: New messages received: %v\n", msg.Body())
		handleMessage(
			msg.Body(),
			queue.GetSuccessFunc(msg),
			queue.GetRetryFunc(msg),
			queue.GetFailFunc(msg),
		)
	}
}

func Loop(queue Queue, handleMessage HandleMessageFunc, stopQueue chan bool) {
	for {
		select {
		case <-stopQueue:
			log.Println("[queue.go] [Loop] Stopping queue worker...")
			return
		default:
			receiveAndProcessMessages(queue, handleMessage)
		}
	}
}

func StartQueueSubscriber(
	queue Queue,
	f HandleMessageFunc,
) func() {
	log.Printf("[queue.go] [StartQueueSubscriber] [Message: Starting queue subscriber]\n")
	stopQueue := make(chan bool)
	for i := 0; i < NUM_WORKERS; i++ {
		go Loop(queue, f, stopQueue)
	}
	return func() {
		stopQueue <- true
	}
}
